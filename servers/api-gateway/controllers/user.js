const user = require('../models').user,
    jwt = require('jsonwebtoken'),
    bcrypt = require('bcrypt');

module.exports = {
    getUsers(req, res) {
        return user
            .findAll()
            .then((users) => res.status(200).send(users))
            .catch((err) => res.status(400).send(err))
    },

    getMe(req, res) {
        const loginInfo = req.user;
        let expiresIn = loginInfo.exp - Math.round(new Date().getTime()/1000);
        expiresIn = expiresIn>0? expiresIn : 0;
        res.status(200).json({ user_name: loginInfo.user_name, expiresIn: expiresIn  });
    },

    getById(req, res) {
        return user
            .findById(req.params.id)
            .then((user) => {
                if (!user) {
                    return res.status(404).json({
                        message: 'User not found'
                    });
                }
                return res.status(200).send(user);
            })
            .catch((error) => res.status(400).send(error));
    },

    register(req, res) {
        return user
            .create({
                user_name: req.body.user_name,
                password_hash: bcrypt.hashSync(req.body.password_hash, 10),
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email_address: req.body.email_address,
                created_by: req.body.created_by,
                created_date: req.body.created_date,
                updated_by: req.body.updated_by,
                updated_date: req.body.updated_date
            })
            .then((user) => res.status(200).send(user))
            .catch((err) => res.status(400).send(err));
    },

    updateUser(req, res) {
        return user
            .findById(req.params.id)
            .then(user => {
                if (!user) {
                    return res.status(404).json({
                        message: 'User not found'
                    });
                }
                return user
                    .update({
                        user_name: req.body.user_name,
                        first_name: req.body.first_name,
                        last_name: req.body.last_name,
                        email_address: req.body.email_address,
                        created_by: req.body.created_by,
                        created_date: req.body.created_date,
                        updated_by: req.body.updated_by,
                        updated_date: req.body.updated_date
                    })
                    .then(() => res.status(200).send(user))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },

    login(req, res) {
        return user
            .findOne({
                where: {
                    user_name: req.body.user_name,
                }
            })
            .then((user) => {
                if (!user) {
                    return res.status(401).json({ message: 'Authentication failed. User not found.' });
                } else {
                    comparePassword = (password) => {
                        return bcrypt.compareSync(password, user.password_hash)
                    }
                    if (comparePassword(req.body.password) == false) {
                        res.status(401).json({ message: 'Authentication failed. Wrong password.' })
                    } else {
                        const token = jwt.sign({ email: user.email_address, 
                            user_name: user.user_name, user_id: user.id }, 
                            'projhash4#', { expiresIn: '5m' });
                        res.send({ token, expiresIn: 300 })
                    }
                }
            })
    },

    delete(req, res) {
        return user
            .findById(req.params.id)
            .then(user => {
                if (!user) {
                    return res.status(400).send({
                        message: 'User Not Found'
                    });
                }
                return user
                    .destroy()
                    .then(() => {
                        res.status(204).json({
                            message: 'User Deleted'
                        })
                        .catch((error) => { res.status(400).send(error) })
                    })
            })
            .catch((error) => res.status(400).send(error));
    },

    loginRequired(req, res, next) {
        if (req.user) {
            next();
        } else {
            return res.status(401).json({ message: 'Unauthorized User!' });
        }
    },

}