const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const jwt = require('jsonwebtoken');
const proxy = require('express-http-proxy');

const indexRouter = require('./routes/index');
// const usersRouter = require('./routes/users');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});


//Middleware for Authorization
app.use(function(req,res,next){
  if(req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT'){
    jwt.verify(req.headers.authorization.split(' ')[1], 'projhash4#', function(err, decode){
      if(err) req.user = undefined;
      req.user = decode;
      app.set('pt_user', JSON.stringify(decode));
      next();
    })
  } else {
    app.set('pt_user',null);
    req.user = undefined;
    next();
  }
});


app.use('/', indexRouter);

// Proxy Middleware to allow all get calls.  POST, UPDATE, AND DELETE have to be authenticated
app.use((req,res, next) => {
  if(req.method == 'GET' || req.method === 'OPTIONS'){
    next();
  } else if(!req.user){
    return res.status(401).json({ 
      message: 'Unauthorized User!'
    });
  } else {
    next();
  }
})

app.use('/api/resources/', proxy('http://localhost:3001',
  {
    proxyReqPathResolver: (req) => {
      return req.url;
    },
    proxyReqOptDecorator: function(proxyReqOpts, srcReq) {
  
      if (app.get('pt_user')) {
        proxyReqOpts.headers['user'] = app.get('pt_user');
      }
      return proxyReqOpts;
    }
  }
))
// app.use('/api/roles/', proxy('http://localhost:3005', {
//   proxyReqPathResolver: (req) => {
//     return req.url;
//   }
// }))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
