DROP DATABASE IF EXISTS "jh-sample-db";
CREATE DATABASE "jh-sample-db";

CREATE TABLE users (
  Id SERIAL PRIMARY KEY,
  first_name VARCHAR,
  last_name VARCHAR,
  email VARCHAR,
  password VARCHAR,  
  createdBy VARCHAR,
  createdOn TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

INSERT INTO users (first_name, last_name, email, password, createdBy)
  VALUES ('Jaya', 'Halegowda', 'jaya@gmail.com', 'password', 'Jaya');
