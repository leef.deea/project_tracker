'use strict';
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    user_name: DataTypes.STRING,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    email_address: DataTypes.STRING,
    password_hash: DataTypes.STRING,
    created_by: DataTypes.STRING,
    created_date: DataTypes.DATE,
    updated_by: DataTypes.STRING,
    updated_date: DataTypes.DATE
  }, {
    schema: 'dee',
    timestamps: false
  });
  user.associate = function(models) {
    // associations can be defined here
  };
  user.sync();
  return user;
};