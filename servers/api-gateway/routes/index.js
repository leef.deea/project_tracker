const express = require('express');
const router = express.Router();

const userController = require('../controllers').user;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

const db = require('../queries');

//Authentication Route
router.get('/api/users', userController.loginRequired, userController.getUsers);
router.get('/api/user/me', userController.loginRequired, userController.getMe )
router.get('/api/user/:id', userController.getById);
router.post('/api/auth/register', userController.register);
router.post('/api/auth/login', userController.login);
router.put('/api/user/:id', userController.updateUser);
router.delete('/api/user/:id', userController.delete);

module.exports = router;

