var promise = require('bluebird');

var options = {
  // Initialization Options
  promiseLib: promise
};

var pgp = require('pg-promise')(options);
var connectionString = 'postgres://postgres:postgres@localhost:5432/jh-sample-db';
var db = pgp(connectionString);

// add query functions

module.exports = {
  getAllUsers: getAllUsers,
  getSingleUser: getSingleUser,
  createUser: createUser,
  updateUser: updateUser,
  removeUser: removeUser
};

function getAllUsers(req, res, next) {
    db.any('select * from users')
      .then(function (data) {
        res.status(200).jsonp(data);
      })
      .catch(function (err) {
        return next(err);
      });
  }
  
  function getSingleUser(req, res, next){
    userEmail = req.params.email;
    db.one('select * from users where email = $1', userEmail)
    .then(function(data){
      res.status(200).jsonp(data);
    })
    .catch(function(err){
      return next(err);
    });
  }
  

  function createUser(req, res, next) {    
    db.none('insert into users(first_name, last_name, email, password, createdby)' +
        'values(${first_name}, ${last_name},${email}, ${password}, ${createdby})',
      req.body)
      .then(function () {
        res.status(200)
          .json({
            status: 'success',
            message: 'Inserted one user'
          });
      })
      .catch(function (err) {
        return next(err);
      });
  }
  
  function updateUser(req, res, next) {
    db.none('update users set first_name=$1, last_name=$2, password=$3, createdby=$4 where email=$5',
      [req.body.first_name, req.body.last_name, req.body.password, 
        req.body.createdby, req.params.email])
      .then(function () {
        res.status(200)
          .json({
            status: 'success',
            message: 'Updated user'
          });
      })
      .catch(function (err) {
        return next(err);
      });
  }
  
  function removeUser(req, res, next) {
    var userEmail = parseInt(req.params.email);
    db.result('delete from users where email = $1', userEmail)
      .then(function (result) {
        res.status(200)
          .json({
            status: 'success',
            message: `Removed ${result.rowCount} user`
          });
      })
      .catch(function (err) {
        return next(err);
      });
  }
  