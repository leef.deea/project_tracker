var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express Resource App' });
});

var db = require('../queries');

router.get('/projects', db.getProjects);
router.get('/projects/:id', db.getSingleProject);
router.post('/projects', db.createProject);
router.put('/projects/:id', db.updateProject);
router.delete('/projects/:id', db.removeProject);

module.exports = router;

