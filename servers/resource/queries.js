var promise = require("bluebird");

var options = {
  // Initialization Options
  promiseLib: promise,
  schema: ["project_tracker", "dee"], /* make both schemas visible */
  query(e) {
    let equery =e.query.toString();
    idx  = equery.toLowerCase().indexOf("select");
    if ( idx < 0 || idx > 10 ) {
      console.log(e.query);
    }
  }
};

var pgp = require("pg-promise")(options);
var connectionString = "postgres://postgres:postgres@localhost:5432/project_tracker";
var db = pgp(connectionString);

// add query functions

module.exports = {
  getProjects: getProjects,
  createProject: createProject,
  getSingleProject: getSingleProject,
  updateProject: updateProject,
  removeProject: removeProject
};

var baseProjectQuery = `SELECT 
    proj.id, mod, app_name, proj_name, project_mgr, kickoff_date, proj.created_by, proj.created_date, proj.updated_by, proj.updated_date,
    pri, amp_lead, gold_team_reqt_lead, reqt_matrix, user_stories, product_roadmap, loe, initial_schedule, spend_plan, cdd, conops, isso_mgr,
    config_mgr, scrum_master, fiscal_year, govt_poc, pop_start_slin_funded, cio2_kickoff, tem_app_pm, loe_submitted, govt_approval,
    publish_baseline_schedule, rmf_kickoff, arr, code_freeze, ato, transitioned_om, finished_planned, finished_forecast, status phase_status,
    schedule, resources, portfolio.budget portfolio_budget, unclass_dev, project_scope, tems, notional_schedule, quality.budget quality_budget,
    frds, project_roadmap, resource_assessment, cio_approval_date, publish_schedule, sprint0_scheduled_finish_date, sprint0_actual_finish_date,
    iter_scrum1_scheduled_finish_date, iter_scrum1_actual_finish_date, code_freeze_scheduled_finish_date, code_freeze_actual_finish_date,
    rmf4_scheduled_finish_date, rmf4_actual_finish_date, rm5_scheduled_finish_date rmf5_scheduled_finish_date, rm5_actual_finish_date rmf5_actual_finish_date,
    applicable_milestones, missed_milestones, finished_milestones, quality.id quality_id, capture.id capture_id, portfolio.id portfolio_id, sprint.id sprint_id,
    proj.created_by = $1 is_editable
  FROM
    dee.proj_details as proj
  LEFT OUTER JOIN dee.capture as capture ON
    proj.id = capture.proj_details_id   
  LEFT OUTER JOIN dee.quality as quality ON
    proj.id = quality.proj_details_id           
  LEFT OUTER JOIN dee.sprint as sprint ON
    sprint.proj_details_id = proj.id
  LEFT OUTER JOIN dee.portfolio as portfolio ON
    portfolio.proj_details_id = proj.id`;

function getProjects(req, res, next) {
  selectQuery = baseProjectQuery;
  let user_name = 'no_users';
  if ( req.headers.user ) {
    user_name = JSON.parse( req.headers.user ).user_name;
  }
  
  db.any(selectQuery, user_name )
    .then(function(data) {
      console.log('created_by ' + data.created_by );
      res.status(200).jsonp(data);
    })
    .catch(function(err) {
      return next(err);
    });

}

function getSingleProject(req, res, next) {
  projId = req.params.id;

  let user_name = 'no_users';
  if ( req.headers.user ) {
    user_name = JSON.parse( req.headers.user ).user_name;
  }
  
  selectQuery = baseProjectQuery + ' WHERE proj.id = $2'; 
  db.one(selectQuery, [user_name, projId])
    .then(function(data) {
      res.status(200).jsonp(data);
    })
    .catch(function(err) {
      console.log( err );
      return next(err);
    });
}


function createProject(req, res, next) {  
  const user_name = JSON.parse( req.headers.user ).user_name;
  req.body.created_by = user_name;
  console.log( "kickoff_date " + req.body.kickoff_date );
    db.tx( t => {
      return t.one(
        'INSERT INTO dee.proj_details(mod, app_name, proj_name, project_mgr, kickoff_date, created_by, updated_by) '+ 
        'VALUES(${mod}, ${app_name}, ${proj_name}, \'project_mgr:todo\', ${kickoff_date}, ${created_by}, ${created_by}) RETURNING id', 
        req.body
      )
      .then( proj => {
        req.body.proj_details_id = proj.id;

        return t.one(
          'INSERT INTO dee.capture( ' + 
              'reqt_matrix, user_stories, product_roadmap, loe, initial_schedule, spend_plan, cdd, conops, ' +
              'isso_mgr, config_mgr, scrum_master, '+
              'created_by, updated_by, proj_details_id ) ' +
            'VALUES( '+
              '${reqt_matrix}, ${user_stories}, ${product_roadmap}, ${loe}, ${initial_schedule}, ${spend_plan}, ${cdd}, ${conops}, ' +
              '${isso_mgr}, ${config_mgr}, ${scrum_master}, '+
              '${created_by}, ${created_by}, ${proj_details_id} )  RETURNING id', 
              req.body
        )
        /*.then( cap => { 
          req.body.capture_id = Number(cap.id);
          return t.one(
            'INSERT INTO dee.slin("number", description, created_by, created_date, updated_by, updated_date, proj_details_id) ' + 
            'VALUES (${number}, ${description}, ${created_by}, ${created_date}, ${updated_by}, ${updated_date}, ${proj_details_id}) RETURNING id', 
            req.body
          )          
        })*/
        .then( cap => {          
          //req.body.slin_id = Number(slin.id);
          req.body.capture_id = Number(cap.id);
          return promise.all([

            t.none('INSERT INTO dee.portfolio(fiscal_year, govt_poc, pop_start_slin_funded, ' + 
                'cio2_kickoff, tem_app_pm, loe_submitted, govt_approval, publish_baseline_schedule, rmf_kickoff, arr, ' +
                'code_freeze, ato, transitioned_om, finished_planned, finished_forecast, status, schedule, ' + 
                'resources, budget, unclass_dev, created_by, updated_by, proj_details_id, capture_id)' +
              'VALUES (${fiscal_year}, ${govt_poc}, ${pop_start_slin_funded}, ' + 
                '${cio2_kickoff}, ${tem_app_pm}, ${loe_submitted}, ${govt_approval}, ${publish_baseline_schedule}, ${rmf_kickoff}, ${arr}, ' + 
                '${code_freeze}, ${ato}, ${transitioned_om}, ${finished_planned}, ${finished_forecast}, ${portfolio_status}, ${schedule}, ' + 
                '${resources}, ${portfolio_budget}, ${unclass_dev}, ${created_by}, ' + 
                '${created_by}, ${proj_details_id}, ${capture_id})', 
              req.body),  
            
            t.none('INSERT INTO dee.quality(project_scope, tems, ' + 
              'notional_schedule, budget, frds, '+
              'project_roadmap, resource_assessment, cio_approval_date, publish_schedule,  ' +
              'created_by, updated_by, proj_details_id) ' +
            'VALUES (${project_scope}, ${tems}, '+
              '${notional_schedule}, ${quality_budget}, ${frds}, ' +
              '${project_roadmap}, ${resource_assessment}, ${cio_approval_date}, ${publish_schedule}, ' +
              '${created_by}, ${created_by}, ${proj_details_id})', 
              req.body),  
          
            t.none('INSERT INTO dee.sprint ( ' + 
              'sprint0_scheduled_finish_date, sprint0_actual_finish_date, iter_scrum1_scheduled_finish_date, ' +
              'iter_scrum1_actual_finish_date, code_freeze_scheduled_finish_date, code_freeze_actual_finish_date, ' +
              'rmf4_scheduled_finish_date, rmf4_actual_finish_date, applicable_milestones, missed_milestones, ' +
              'finished_milestones, rm5_scheduled_finish_date, rm5_actual_finish_date, ' +
              'created_by, updated_by, proj_details_id )' +
            'VALUES (  ' + 
              '${sprint0_scheduled_finish_date}, ${sprint0_actual_finish_date}, ${iter_scrum1_scheduled_finish_date}, ' +
              '${iter_scrum1_actual_finish_date}, ${code_freeze_scheduled_finish_date}, ${code_freeze_actual_finish_date}, ' +
              '${rmf4_scheduled_finish_date}, ${rmf4_actual_finish_date}, ${applicable_milestones}, ${missed_milestones}, ' +
              '${finished_milestones}, ${rmf5_scheduled_finish_date}, ${rmf5_actual_finish_date}, '+
              '${created_by}, ${created_by}, ${proj_details_id})', 
              req.body),  
          ]);
        });
        

      })
    })
    .then(function (data) {
      console.log( 'success' );
        // Success, do something with data...
        return res.status(200).json({success: true, proj_details_id: req.body.proj_details_id});
    }, function (reason) {
        // Error
        console.error( reason );
        return res.json(reason);
    });
}

function updateProject(req, res, next) {
  const user_name = JSON.parse( req.headers.user ).user_name;
  req.body.updated_by = user_name;
  db.tx( t => {
    return t.batch([
      t.none(
        'UPDATE dee.proj_details SET '+
        'mod = ${mod}, app_name=${app_name}, proj_name=${proj_name}, ' +
        'project_mgr=${project_mgr}, kickoff_date=${kickoff_date}, updated_by=${updated_by} ' + 
        'WHERE id = ${id} ',
        req.body
      ),
      t.none(
        'UPDATE dee.quality SET ' +
          'project_scope=${project_scope}, tems=${tems}, ' +
          'notional_schedule=${notional_schedule}, budget= ${quality_budget},  ' +
          'frds=${frds}, project_roadmap=${project_roadmap},  ' +
          'resource_assessment=${resource_assessment}, cio_approval_date=${cio_approval_date},  ' +
          'publish_schedule=${publish_schedule}, updated_by=${updated_by} ' +
        'WHERE id = ${quality_id} ',
        req.body
      ),
      t.none(
        'UPDATE dee.sprint SET ' +
          'sprint0_scheduled_finish_date=${sprint0_scheduled_finish_date}, sprint0_actual_finish_date=${sprint0_actual_finish_date}, '+
          'iter_scrum1_scheduled_finish_date=${iter_scrum1_scheduled_finish_date}, iter_scrum1_actual_finish_date=${iter_scrum1_actual_finish_date}, '+
          'code_freeze_scheduled_finish_date=${code_freeze_scheduled_finish_date}, code_freeze_actual_finish_date=${code_freeze_actual_finish_date}, ' +
          'rmf4_scheduled_finish_date=${rmf4_scheduled_finish_date}, rmf4_actual_finish_date=${rmf4_actual_finish_date}, '+
          'applicable_milestones=${applicable_milestones}, missed_milestones=${missed_milestones}, finished_milestones=${finished_milestones}, ' +
          'rm5_scheduled_finish_date=${rmf5_scheduled_finish_date}, rm5_actual_finish_date=${rmf5_actual_finish_date}, updated_by=${updated_by}' +
        'WHERE id = ${sprint_id} ',
        req.body
      ),
      t.none(
        'UPDATE dee.capture SET ' +
          'reqt_matrix=${reqt_matrix}, user_stories=${user_stories}, product_roadmap=${product_roadmap}, '+
          'loe=${loe}, initial_schedule=${initial_schedule}, spend_plan=${spend_plan}, cdd=${cdd}, ' +
          'conops=${conops}, isso_mgr=${isso_mgr}, config_mgr=${config_mgr}, scrum_master=${scrum_master}, '+
          'updated_by=${updated_by}' +
        'WHERE id = ${capture_id} ',
        req.body
      ),
      t.none(
        'UPDATE dee.portfolio SET ' +
          'fiscal_year=${fiscal_year}, govt_poc=${govt_poc}, pop_start_slin_funded=${pop_start_slin_funded}, ' + 
          'cio2_kickoff=${cio2_kickoff}, tem_app_pm=${tem_app_pm}, loe_submitted=${loe_submitted}, govt_approval=${govt_approval}, '+
          'publish_baseline_schedule=${publish_baseline_schedule}, rmf_kickoff=${rmf_kickoff}, arr=${arr}, ' +
          'code_freeze=${code_freeze}, ato=${ato}, transitioned_om=${transitioned_om}, finished_planned=${finished_planned}, ' +
          'finished_forecast=${finished_forecast}, schedule=${schedule}, ' + 
          'resources=${resources}, budget=${portfolio_budget}, unclass_dev=${unclass_dev}, ' +
          'updated_by=${updated_by} ' +
        'WHERE id = ${portfolio_id} ',
        req.body
      )
    ]);
  })
  .then(function() {
      res.status(200).json({
        status: "success",
        message: "Updated project"
      });
  })
    .catch(function(err) {
      console.error( reason );
      return next(err);
  });

}

function removeProject(req, res, next) {
  var ProjId = parseInt(req.params.ID);
  db.result("delete from proj_details where id = $1", ProjId)
    .then(function(result) {
      res.status(200).json({
        status: "success",
        message: `Removed ${result.rowCount} project`
      });
    })
    .catch(function(err) {
      return next(err);
    });
}
