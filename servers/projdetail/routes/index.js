const express = require('express');
const router = express.Router();

const proj_detailController = require('../controllers').proj_detail;
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/api/proj_details', proj_detailController.getDetails);
router.get('/api/proj_details/:id', proj_detailController.getById);
router.post('/api/proj_details/create', proj_detailController.create);
router.put('/api/proj_details/:id', proj_detailController.updateDetails);
router.delete('/api/proj_details/:id', proj_detailController.delete);

module.exports = router;
