const proj_detail = require('../models').proj_detail

module.exports = {
    getDetails(req, res) {
        return proj_detail
            .findAll()
            .then((proj_details) => res.status(200).send(proj_details))
            .catch((err) => res.status(400).send(err))
    },

    getById(req, res) {
        return proj_detail
            .findById(req.params.id)
            .then((proj_detail) => {
                if (!proj_detail) {
                    return res.status(404).json({
                        message: 'proj_detail not found'
                    });
                }
                return res.status(200).send(proj_detail);
            })
            .catch((error) => res.status(400).send(error));
    },

    create(req, res) {
        return proj_detail
            .create({
                id: req.body.id,
                mod: req.body.mod,
                app_name: req.body.app_name,
                proj_name: req.body.proj_name,
                project_mgr: req.body.project_mgr,
                kickoff_date: req.body.kickoff_date,
                created_by: req.body.created_by,
                created_date: req.body.created_date,
                updated_by: req.body.updated_by,
                updated_date: req.body.updated_date,
                createdAt: req.body.createdAt,
                updatedAt: req.body.updatedAt
            })
            .then((proj_detail) => res.status(200).send(proj_detail))
            .catch((err) => res.status(400).send(err));
    },

    updateDetails(req, res) {
        return proj_detail
            .findById(req.params.id)
            .then(proj_detail => {
                if (!proj_detail) {
                    return res.status(404).json({
                        message: 'proj_detail not found'
                    });
                }
                return proj_detail
                    .update({
                        id: req.body.id,
                        mod: req.body.mod,
                        app_name: req.body.app_name,
                        proj_name: req.body.proj_name,
                        project_mgr: req.body.project_mgr,
                        kickoff_date: req.body.kickoff_date,
                        created_by: req.body.created_by,
                        created_date: req.body.created_date,
                        updated_by: req.body.updated_by,
                        updated_date: req.body.updated_date,
                        createdAt: req.body.createdAt,
                        updatedAt: req.body.updatedAt
                    })
                    .then(() => res.status(200).send(proj_detail))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },


    delete(req, res) {
        return proj_detail
            .findById(req.params.id)
            .then(proj_detail => {
                if (!proj_detail) {
                    return res.status(400).send({
                        message: 'proj_detail Not Found'
                    });
                }
                return proj_detail
                    .destroy()
                    .then(() => res.status(204).json({
                        message: "Project Detail Deleted"
                    }))
                    .catch((error) => { res.status(400).send(error) })
                   
            })
            .catch((error) => res.status(400).send(error));
    },



}