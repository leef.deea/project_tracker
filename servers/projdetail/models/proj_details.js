'use strict';
module.exports = (sequelize, DataTypes) => {
  const proj_detail = sequelize.define('proj_detail', {
    mod: DataTypes.STRING,
    app_name: DataTypes.STRING,
    proj_name: DataTypes.STRING,
    project_mgr: DataTypes.STRING,
    kickoff_date: DataTypes.DATE,
    created_by: DataTypes.STRING,
    created_date: DataTypes.DATE,
    updated_by: DataTypes.STRING,
    updated_date: DataTypes.DATE
  }, {
    schema: 'dee'
  });
  proj_detail.associate = function(models) {
    // associations can be defined here
  };
  proj_detail.sync();
  return proj_detail;
};

