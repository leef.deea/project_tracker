'use strict';
module.exports = (sequelize, DataTypes) => {
  var proj_role = sequelize.define('proj_role', {
    proj_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    role: DataTypes.STRING
  }, {
    schema: 'dee'
  });
  proj_role.associate = function(models) {
    // associations can be defined here
  };
  proj_role.sync();
  return proj_role;
};