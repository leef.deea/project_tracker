const proj_role = require('../models').proj_role

module.exports = {
    getRoles(req, res) {
        return proj_role
            .findAll()
            .then((proj_role) => res.status(200).send(proj_role))
            .catch((err) => res.status(400).send(err))
    },


    getById(req, res) {
        return proj_role
            .findById(req.params.id)
            .then((proj_role) => {
                if (!proj_role) {
                    return res.status(404).json({
                        message: 'proj_role not found'
                    });
                }
                return res.status(200).send(proj_role);
            })
            .catch((error) => res.status(400).send(error));
    },

    createRole(req, res) {
        return proj_role
            .create({
                proj_id: req.body.proj_id,
                user_id: req.body.user_id,
                role: req.body.role
            })
            .then((proj_role) => res.status(200).send(proj_role))
            .catch((err) => res.status(400).send(err));
    },

    updateRole(req, res) {
        return proj_role
            .findById(req.params.id)
            .then(proj_role => {
                if (!proj_role) {
                    return res.status(404).json({
                        message: 'proj_role not found'
                    });
                }
                return proj_role
                    .update({
                        proj_id: req.body.proj_id,
                        user_id: req.body.user_id,
                        role: req.body.role
                    })
                    .then(() => res.status(200).send(proj_role))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },

   

    delete(req, res) {
        return proj_role
            .findById(req.params.id)
            .then(proj_role => {
                if (!proj_role) {
                    return res.status(400).send({
                        message: 'proj_role Not Found'
                    });
                }
                return proj_role
                    .destroy()
                    .then(() => {
                        res.status(204).json({
                            message: 'proj_role Deleted'
                        })
                            .catch((error) => { res.status(400).send(error) })
                    })
            })
            .catch((error) => res.status(400).send(error));
    },

    

}