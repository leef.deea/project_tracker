const express = require('express');
const router = express.Router();

const roleController = require('../controllers').proj_role

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/api/proj_role', roleController.getRoles);
router.get('/api/proj_role/:id', roleController.getById)
router.post('/api/proj_role', roleController.createRole);
router.put('/api/proj_role/:id', roleController.updateRole);
router.delete('/api/proj_role/:id', roleController.delete);

module.exports = router;
