import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { ValidateToken } from './auth/store/auth.action';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Project Tracker';
  constructor(private store: Store) {
    store.dispatch( new ValidateToken() );
  }
}
