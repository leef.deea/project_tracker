import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { CanDeactivateGuard } from './can-deactivate-guard';
import { ProjectListComponent } from './projects/project-list/project-list.component';


const appRoutes: Routes = [

  { path: '', component: ProjectListComponent},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent, canDeactivate: [CanDeactivateGuard] }

];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
