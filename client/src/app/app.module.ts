import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';

import { AppRoutingModule } from './app-routing.module';
import { ModalModule } from 'ngx-modialog';
import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';

import { AppComponent } from './app.component';

import { ProjectState } from './projects/store/project.state';

import { AuthState } from './auth/store/auth.state';
import { LoginComponent } from './auth/login/login.component';

import { MsgState } from './header/msg.state';
import { AuthInterceptor } from './auth/auth.interceptor';
import { CanDeactivateGuard } from './can-deactivate-guard';

import { RegisterComponent } from './auth/register/register.component';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';

import { ColState } from './col-select/store/col.state';
import { ProjectsModule } from './projects/projects.module';
import { HeaderModule } from './header/header.module';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HeaderModule,
    AppRoutingModule,
    HttpClientModule,
    NgxsModule.forRoot([
      ProjectState,
      AuthState,
      MsgState,
      ColState
    ]),
    ModalModule.forRoot(),
    BootstrapModalModule,
    NgxsStoragePluginModule.forRoot({
      key: ['auth.token']
     // key: ['auth.token', 'column.columns']
    }),
    BootstrapModalModule,
    NgxsReduxDevtoolsPluginModule.forRoot(),
    ProjectsModule

  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    CanDeactivateGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
