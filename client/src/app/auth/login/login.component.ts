import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Store } from '@ngxs/store';
import { Login } from '../store/auth.action';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {
  username: string;
  password: string;

  @ViewChild('focusthis') vc: ElementRef;


  constructor( private store: Store ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.vc.nativeElement.focus();
  }

  login() {
    // this.toastr.success('Login this...', 'AAaa');
    this.store.dispatch( new Login( this.username, this.password) );

  }
}
