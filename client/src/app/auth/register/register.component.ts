import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngxs/store';
import { Register } from '../store/auth.action';
import { CanComponentDeactivate } from '../../can-deactivate-guard';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, CanComponentDeactivate {
  @ViewChild('f') f: any;
  user_name: string;
  password_hash: string;
  vpassword: string;
  email_address: string;
  first_name: string;
  last_name: string;


  constructor(private store: Store, private modal: Modal) { }

  ngOnInit() {
  }

  register() {
    this.store.dispatch( new Register( {
      user_name: this.user_name,
      password_hash: this.password_hash,
      email_address: this.email_address,
      first_name: this.first_name,
      last_name: this.last_name
    }) );

    this.f.form.markAsPristine();

  }
  canDeactivate() {
    if (this.f.form.pristine) {
      return true;
    }

    const dialogRef = this.modal
      .confirm()
      .title('')
      .headerClass('modal-header-flee')
      .footerClass('modal-footer-flee')
      .body(`Press OK to abandon changes.`)
      .open();

    return dialogRef.result;

  }

}
