import { State, Action, StateContext, Store, Selector } from '@ngxs/store';

import { Login, LoginSuccess, Logout, Register, ValidateToken, StartExpireInTimer } from './auth.action';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Msg, MsgType } from '../../header/msg.state';
import { environment } from '../../../environments/environment.prod';

​
@Injectable()

export class AuthStateModel {
  user: string;
  token: string;
}

@State<AuthStateModel>({
  name: 'auth',
  defaults: {
    user: null,
    token: null
  }
})

export class AuthState {

  tokenTimer: any;
  apiBaseUrl = environment.apiBaseUrl;
  constructor( private httpClient: HttpClient, private store: Store, private toastr: ToastrService ) {}

  @Selector() static isAuthorized( state ) {
    return state.token !== null;
  }

  @Selector() static getToken( state ): string {
    return state.token;
  }

  @Action(Login)
  login(ctx: StateContext<AuthStateModel>, action: Login ) {

    this.httpClient.post( this.apiBaseUrl + 'auth/login',
      {'user_name': action.user, 'password': action.password},
      {observe: 'response'}
    ).subscribe( ( data ) => {
        if ( data.status === 200 ) {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            user: action.user
          });
          const expiresIn: number = (<any>data.body).expiresIn;
          this.store.dispatch( [new StartExpireInTimer( expiresIn ),
            new LoginSuccess( 'JWT ' + (<any>data.body).token )]);
        }
    },
    error => {
      console.log( 'Error Login data ');
      this.store.dispatch( new Msg(MsgType.error, 'Invalid login creditential.') );
    }

   );
  }

  @Action(LoginSuccess)
  loginSuccess( ctx: StateContext<AuthStateModel>, action: LoginSuccess) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      ...action
    });
    this.store.dispatch(new Msg(MsgType.success, 'User Logged In', '/projects'));
  }

  @Action(Logout)
  logout( ctx: StateContext<AuthStateModel>, action: Logout) {
    const state = ctx.getState();
    ctx.setState({
      user: null,
      token: null
    });
    if ( this.tokenTimer ) {
      clearTimeout(this.tokenTimer);
    }
    this.store.dispatch(new Msg(MsgType.success, 'User Logged Out'));
  }

  @Action(Register)
  register(ctx: StateContext<AuthStateModel>, action: Register ) {

    this.httpClient.post( this.apiBaseUrl + 'auth/register',
      {
        'user_name': action.payload.user_name,
        'password_hash': action.payload.password_hash,
        'email_address': action.payload.email_address,
        'first_name': action.payload.first_name,
        'last_name': action.payload.last_name
      },
      {observe: 'response'}
    ).subscribe( ( data ) => {
        if ( data.status === 200 ) {
          this.store.dispatch( new Msg(MsgType.success, 'Registered user. Please login w/ your new credentials', '/login') );
        }
    },
    error => {
      this.store.dispatch( new Msg(MsgType.error, 'Invalid login creditential.') );
    }
   );

  }

  @Action(ValidateToken)
  validateToken( ctx: StateContext<AuthStateModel>, action: ValidateToken ) {
    this.httpClient.get<any>( this.apiBaseUrl + 'user/me', {observe: 'response'} ).subscribe(
      (data) => {
       ctx.patchState( {
        user: data.body.username
       });
       ctx.dispatch( new StartExpireInTimer(data.body.expiresIn));
      },
      error => {
        ctx.setState({
          user: null,
          token: null
        });
      }
    );
  }

  @Action(StartExpireInTimer)
  startExpireInTimer( ctx: StateContext<AuthStateModel>, action: StartExpireInTimer ) {

    const duration = action.payload;
    console.log('Setting timer: ' + duration);
    this.tokenTimer = setTimeout(() => {
       this.store.dispatch( [new Msg(MsgType.error, 'Token Expired.'), new Logout()]);

    }, duration * 1000);

  }

}
