

export class Login {
  static readonly type = '[Auth] Login';
  constructor(public user: string, public password: string ) {}
}

export class LoginSuccess {
  static readonly type = '[Auth] Login Success';
  constructor(public token: string) {}
}

export class Logout {
  static readonly type = '[Auth] Logout';
}


export class LoginFailed {
  static readonly type = '[Auth] Login Failed';
}

export class Register {
  static readonly type = '[Auth] Register';
  constructor(public payload: {
    user_name: string,
    password_hash: string,
    email_address: string,
    first_name: string,
    last_name: string
  } ) {}
}

export class ValidateToken {
  static readonly type = '[Auth] Validate';
}

export class StartExpireInTimer {
  static readonly type = '[Auth] Start Expire In Timer';
  constructor( public payload: number ) {}
}

export class TokenExpired {
  static readonly type = '[Auth] Expired Token Timer';
}
