import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectComponent } from './project-edit/project.component';
import { CanDeactivateGuard } from '../can-deactivate-guard';
import { QualityFormComponent } from './project-edit/quality-form/quality-form.component';
import { SprintFormComponent } from './project-edit/sprint-form/sprint-form.component';
import { CaptureFormComponent } from './project-edit/capture-form/capture-form.component';
import { PortfolioFormComponent } from './project-edit/portfolio-form/portfolio-form.component';
import { RoleFormComponent } from './project-edit/role-form/role-form.component';


const appRoutes: Routes = [

  { path: '', component: ProjectListComponent},
  { path: 'projects', component: ProjectListComponent},
  { path: 'project/:id', component: ProjectComponent, canDeactivate: [CanDeactivateGuard],
    children: [
      { path: '', redirectTo: 'quality', pathMatch: 'full' },
      { path: 'quality', component: QualityFormComponent },
      { path: 'sprint', component: SprintFormComponent },
      { path: 'capture', component: CaptureFormComponent },
      { path: 'portfolio', component: PortfolioFormComponent },
      { path: 'role', component: RoleFormComponent }
  ]}
];

@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
