import { Project } from './project.model';

export class AddProject {
  static readonly type = '[Project] Add';
  constructor( public payload: Project ) {}
}

export class UpdateProject {
  static readonly type = '[Project] Update';
  constructor( public payload: Project ) {}
}

export class LoadProject {
  static readonly type = '[Project] Load';
  constructor( public payload: number ) {}
}

export class LoadAllProject {
  static readonly type = '[Project] Load All';
}

export class DeleteProject {
  static readonly type = '[Project] Delete';
  constructor( public id: number ) {}
}

export class LoadProjectSuccess {
  static readonly type = '[Project] Load Success';
  constructor( public payload: Project ) {}
}

export class ActionFailed {
  static readonly type = '[Project] Failure';
  constructor( public payload: Error ) {}
}

export class SaveSuccess {
  static readonly type = '[Project] Save Success';
  constructor( public id: number ) {}
}
