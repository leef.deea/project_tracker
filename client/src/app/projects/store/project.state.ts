import { State, Action, StateContext, Store } from '@ngxs/store';
import { Project } from './project.model';
import { AddProject, LoadProject, LoadAllProject, DeleteProject, LoadProjectSuccess, ActionFailed, UpdateProject, SaveSuccess } from './project.action';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, filter } from 'rxjs/operators';
import { Msg, MsgType } from '../../header/msg.state';
import { environment } from '../../../environments/environment.prod';

​

@Injectable()

export class ProjectStateModel {
  loaded: boolean;
  projects: Project[];
}

@State<ProjectStateModel>({
  name: 'projects',
  defaults: {
    loaded: false,
    projects: []
  }
})

export class ProjectState {

  apiBaseUrl = environment.apiBaseUrl + 'resources/';

  constructor( private httpClient: HttpClient, private store: Store ) {}

  @Action(AddProject)
  addProject(ctx: StateContext<ProjectStateModel>, action: AddProject ) {
    const state = ctx.getState();
    this.httpClient.post(this.apiBaseUrl + 'projects', action.payload, { responseType: 'text' } ).subscribe(
      (data: string) => {

        const r = JSON.parse(data);
        if ( r.proj_details_id ) {
          const newId = +r.proj_details_id;
          this.store.dispatch( [
            new SaveSuccess(newId),
            new LoadProject(newId)
          ]);
        }
      }
    );
  }


  @Action(LoadProject)
  loadProject(ctx: StateContext<ProjectStateModel>, action: LoadProject ) {
    const id: number = +action.payload;
    this.httpClient.get<Project>(this.apiBaseUrl + 'projects/' + id).subscribe(
      (project: Project) => {
        console.log( 'Load Project' );
        const state = ctx.getState();
        // filter out existing.
        let idx = 0;
        const clone = state.projects.slice(0);

        const found = clone.some( (c) => {
          console.log('found ' + id + ' ' + c.id );
          if ( +c.id === id) {
            clone[idx] = project;
            return true;
          }
          idx++;
        });
        if ( !found ) {
          clone.push(project);
        }

        ctx.patchState( {
          projects: clone
        });
        this.store.dispatch([ new LoadProjectSuccess(project)]);

      },
      (error) => {
        this.store.dispatch(new Msg(MsgType.error, error.message));
      }
    );
  }


  @Action(LoadAllProject)
  loadAllProject( ctx: StateContext<ProjectStateModel>) {
    this.httpClient.get<Project[]>(this.apiBaseUrl + 'projects').pipe(
      tap( (projects) => {
        ctx.setState( {
          ...ctx.getState(),
          loaded: true,
          projects: projects
        });
      }
    )).subscribe();

  }

  @Action(DeleteProject)
  deleteProject( ctx: StateContext<ProjectStateModel>, action: DeleteProject) {

    this.httpClient.delete(this.apiBaseUrl + 'projects/' + action.id, { responseType: 'text'} ).subscribe(
      (result) => {
        ctx.setState( {
          ...ctx.getState(),
          projects: ctx.getState().projects.filter( (project) => project.id !== action.id )
        });
      },
      (error) => {
        this.store.dispatch(new Msg(MsgType.error, JSON.parse(error.error).message));
      }
    );

  }

  @Action(UpdateProject)
  updateProject( ctx: StateContext<ProjectStateModel>, action: UpdateProject ) {

    const project = action.payload;
    this.httpClient.put(
      this.apiBaseUrl + 'projects/' +  project.id,
      project,
      { responseType: 'text' }
    ).subscribe(
      () => {
        // success... time to update my store.
        const state = ctx.getState();
        let idx = 0;
        const clone = state.projects.slice(0);
        clone.some( (c) => {
          if (c.id === +action.payload.id) {
            clone[idx] = action.payload;
            return true;
          }
          idx++;
        });
        ctx.setState( {
          ...ctx.getState(),
          projects: clone
        });
        this.store.dispatch([new SaveSuccess(project.id)]);

      },
      error => {
        this.store.dispatch(new Msg(MsgType.error, JSON.parse(error.error).message));
      }
    );
  }



}
