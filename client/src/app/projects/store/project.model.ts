import { Audit } from '../../shared/shared.module';

// ** Note
// where is government POC?
// whatis Budget Date?

export class Project extends Audit {
    public id: number;
    public proj_name: string;

    public sprint_id: number;
    public capture_id: number;
    public portfolio_id: number;
    public quality_id: number;

    public mod: string;
    public app_name: string;
    public kickoff_date: Date;

    public project_scope: string;
    public tems: string;

    public quality_budget: string;
    public frds: string;
    public project_roadmap: string;
    public resource_assessment: string;
    public project_mgr = 'project_mgr:todo';

    public sprint0_scheduled_finish_date: Date;
    public sprint0_actual_finish_date: Date;
    public iter_scrum1_scheduled_finish_date: Date;
    public iter_scrum1_actual_finish_date: Date;
    public code_freeze_scheduled_finish_date: Date;
    public code_freeze_actual_finish_date: Date;
    public rmf4_scheduled_finish_date: Date;
    public rmf4_actual_finish_date: Date;
    public rmf5_scheduled_finish_date: Date;
    public rmf5_actual_finish_date: Date;
    public applicable_milestones: string;
    public missed_milestones: string;
    public finished_milestones: string;

    public gold_team_reqt_lead: string;
    public reqt_matrix: string;
    public user_stories: string;
    public product_roadmap: string;
    public loe: string;
    public initial_schedule: string;
    public spend_plan: string;
    public cdd: string;
    public conops: string;
    public isso_mgr: string;
    public config_mgr: string;
    public scrum_master: string;
    public developer1: string;
    public developer2: string;
    public developer3: string;
    public developer4: string;
    public developer5: string;
    public developer6: string;

    public fiscal_year = 2018;
    public govt_poc = 'govt_poc:todo';
    public portfolio_status = 'portfolio_status:todo';
    public unclass_dev = 'unclass_dev:todo';

    public publish_schedule: Date;
    public cio_approval_date: Date;
    public notional_schedule: Date;
    public pop_start_slin_funded: Date;
    public cio2_kickoff: Date;
    public tem_app_pm: Date;
    public loe_submitted: Date;
    public govt_approval: Date;
    public publish_baseline_schedule: Date;
    public rmf_kickoff: Date;
    public arr: Date;
    public code_freeze: Date;
    public ato: Date;
    public transitioned_om: Date;
    public finished_planned: Date;
    public finished_forecast: Date;
    public phase_status: Date;
    public schedule: Date;
    public resources: string;
    public portfolio_budget: String; // ????
    public comment: String;
    public is_editable = false;

    constructor(id: number, name: string, createdBy: string) {
        super();
        this.id = id;
        this.proj_name = name;
        this.created_by = createdBy;
        this.created_date = new Date();
        this.is_editable = true;
    }

}

export class ColType {
  static colType = {
    id: 'number',
    sprint_id: 'number',
    capture_id: 'number',
    portfolio_id: 'number',
    quality_id: 'number',
    kickoff_date: 'date',
    pop_start_slin_funded: 'date',
    cio2_kickoff: 'date',
    tem_app_pm: 'date',
    loe_submitted: 'date',
    govt_approval: 'date',
    publish_baseline_schedule: 'date',
    rmf_kickoff: 'date',
    arr: 'date',
    code_freeze: 'date',
    ato: 'date',
    transitioned_om: 'date',
    finished_planned: 'date',
    finished_forecast: 'date',
    phase_status: 'date',
    schedule: 'date',
    notional_schedule: 'date',
    cio_approval_date: 'date',
    publish_schedule: 'date',
    sprint0_scheduled_finish_date: 'date',
    sprint0_actual_finish_date: 'date',
    iter_scrum1_scheduled_finish_date: 'date',
    iter_scrum1_actual_finish_date: 'date',
    code_freeze_scheduled_finish_date: 'date',
    code_freeze_actual_finish_date: 'date',
    rmf4_scheduled_finish_date: 'date',
    rmf4_actual_finish_date: 'date',
    rmf5_scheduled_finish_date: 'date',
    rmf5_actual_finish_date: 'date',
  };

  static getDataType( col ): string {
    if ( this.colType[col] ) {
      return this.colType[col];
    }
    return 'string';
  }
}
