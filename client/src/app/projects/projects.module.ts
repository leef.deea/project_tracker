
import { NgModule } from '@angular/core';
import { AgGridModule } from 'ag-grid-angular';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProjectListComponent } from './project-list/project-list.component';
import { ColSelectComponent } from '../col-select/col-select.component';
import { ProjectComponent } from './project-edit/project.component';
import { QualityFormComponent } from './project-edit/quality-form/quality-form.component';
import { SprintFormComponent } from './project-edit/sprint-form/sprint-form.component';
import { CaptureFormComponent } from './project-edit/capture-form/capture-form.component';
import { PortfolioFormComponent } from './project-edit/portfolio-form/portfolio-form.component';
import { RoleFormComponent } from './project-edit/role-form/role-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProjectRoutingModule } from './project-routing.module';


@NgModule({

  declarations: [
    ProjectListComponent,
    ColSelectComponent,
    ProjectComponent,
    QualityFormComponent,
    SprintFormComponent,
    CaptureFormComponent,
    PortfolioFormComponent,
    RoleFormComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgbModule,
    ProjectRoutingModule,
    AgGridModule.withComponents([])
  ]

})


export class ProjectsModule {}


