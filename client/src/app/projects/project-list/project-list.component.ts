import { Component, OnInit, ViewChild } from "@angular/core";
import { GridOptions } from "ag-grid";
import { AgGridNg2 } from "ag-grid-angular";
import { Select, Store } from "@ngxs/store";
import { AuthState } from "../../auth/store/auth.state";
import { Observable } from "rxjs";
import { Router } from "@angular/router";
import { Modal } from "ngx-modialog/plugins/bootstrap";
import { Project } from "../store/project.model";
import { LoadAllProject, DeleteProject } from "../store/project.action";
import { ProjectStateModel, ProjectState } from "../store/project.state";
import { ColState, ColStateModel } from "../../col-select/store/col.state";
import { ColModel } from "../../col-select/store/col.model";
import { debounce, debounceTime } from "rxjs/operators";

@Component({
  selector: "app-project-list",
  templateUrl: "./project-list.component.html",
  styleUrls: ["./project-list.component.css"]
})
export class ProjectListComponent implements OnInit {
  showColSelection = false;
  @ViewChild("agGrid")
  agGrid: AgGridNg2;
  private gridApi;
  private gridColApi;
  rowSelection;
  selectedId: number;
  @Select(AuthState.isAuthorized)
  authorized$: Observable<string>;
  @Select(state => state.projects.projects)projects$: Observable<ProjectStateModel>;
  @Select(ColState.getColumns) colVis: Observable<ColModel[]>;

  columnDefs: any[];
  // [
  //   {headerName: 'Id', field: 'id' },
  //   {headerName: 'mod', field: 'mod' },
  //   {headerName: 'Application Name', field: 'app_name' },
  //   {headerName: 'proj_name', field: 'proj_name' },
  //   {headerName: 'project_mgr', field: 'project_mgr' },
  //   {headerName: 'kickoff_date', field: 'kickoff_date' },
  //   {headerName: 'created_by', field: 'created_by' },
  //   {headerName: 'created_date', field: 'created_date' },
  //   {headerName: 'updated_by', field: 'updated_by' },
  //   {headerName: 'updated_date', field: 'updated_date'},
  // ];

  gridOptions = <GridOptions>{
    enableSorting: true,
    enableFilter: true,
    // pagination: true,
    paginationPageSize: 10,
    suppressPaginationPanel: false,
    editable: true,
    floatingFilter: false,
    enableColResize: true,
    rowSelection: 'single',
    // columnDefs: this.columnDefs
  };

  constructor(
    private store: Store,
    private router: Router,
    private modal: Modal
  ) {
    const storedCol = store.selectSnapshot( ColState.getColumns );
    this.columnDefs = storedCol.filter( (col) => col.type === 'c' ).map( (col) => {
      return { headerName: col.column_name, field: col.column_abbr, hide: !col.showx, width: col.width };
    });
    this.gridOptions.columnDefs = this.columnDefs;
  }

  ngOnInit() {
    const projectState = this.store.selectSnapshot(state => state.projects);
    if (!projectState.loaded) {
      this.store.dispatch(new LoadAllProject());
    }

    this.colVis.pipe(debounceTime(1000)).subscribe( columns => {
      if ( this.gridApi ) {
        // const colDefs = [];
        // columns.filter( (col) => col.showx ).forEach( (col) => {
        //   colDefs.push( {headerName: col.column_name, field: col.column_abbr } );
        // });
        // this.gridApi.setColumnDefs(colDefs);

        columns.forEach( (col) => {
          this.gridColApi.setColumnVisible(col.column_abbr, col.showx);
        });
      }
    });

  }

  add() {
    this.router.navigateByUrl('/project/new');
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColApi = params.columnApi;
  }

  onSelectionChanged() {
    const me = this;
    const selectedRows = this.gridApi.getSelectedRows();
    selectedRows.forEach(function(selectedRow, index) {
      me.selectedId = selectedRow.id;
    });
  }
  onDoubleRowDblClk(params) {
    console.log(params);
    this.edit(params.data.id);
  }

  edit(id: number) {
    if (!id) {
      id = this.selectedId;
    }
    this.router.navigateByUrl("/project/" + id);
  }

  delete() {
    const dialogRef = this.modal
      .confirm()
      .title(" ")
      .headerClass("modal-header-flee")
      .footerClass("modal-footer-flee")
      .body(`Press OK to delete Project.`)
      .open();

    dialogRef.result.then(result => {
      this.store.dispatch(new DeleteProject(this.selectedId));
    });
  }

  showHideColSelection() {
    this.showColSelection = !this.showColSelection;
    console.log( this.showColSelection );
  }

}
