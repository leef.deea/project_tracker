import { Component, OnInit } from '@angular/core';
import { FormGroupDirective, ControlContainer, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-role-form',
  templateUrl: './role-form.component.html',
  styleUrls: ['./role-form.component.css'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective }
  ]
})
export class RoleFormComponent implements OnInit {
  private form: FormGroup;
  constructor(private parent: FormGroupDirective) { }

  ngOnInit() {
    this.form = this.parent.form;
  }
}
