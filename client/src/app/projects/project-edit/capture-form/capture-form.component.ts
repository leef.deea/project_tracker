import { Component, OnInit } from '@angular/core';
import { FormGroupDirective, ControlContainer, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-capture-form',
  templateUrl: './capture-form.component.html',
  styleUrls: ['./capture-form.component.css'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective }
  ]
})
export class CaptureFormComponent implements OnInit {
  private form: FormGroup;
  constructor(private parent: FormGroupDirective) { }

  ngOnInit() {
    this.form = this.parent.form;
  }
}