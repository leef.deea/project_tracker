import { Component, OnInit } from '@angular/core';
import { FormGroupDirective, ControlContainer, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-quality-form',
  templateUrl: './quality-form.component.html',
  styleUrls: ['./quality-form.component.css'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective }
  ]
})
export class QualityFormComponent implements OnInit {
  private form: FormGroup;
  constructor(private parent: FormGroupDirective) { }

  ngOnInit() {
    this.form = this.parent.form;
  }
}
