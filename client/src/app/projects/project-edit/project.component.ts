import { Component, OnInit } from '@angular/core';
import { Store, Actions, ofActionSuccessful, ofActionDispatched, ofActionErrored, Select } from '@ngxs/store';
import { ActivatedRoute, Router, CanDeactivate } from '@angular/router';
import { Project, ColType } from '../store/project.model';
import { AddProject, LoadProject,
  DeleteProject, LoadProjectSuccess,
  ActionFailed, UpdateProject,
  SaveSuccess } from '../store/project.action';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AuthState } from '../../auth/store/auth.state';
import { Observable, Subscription } from 'rxjs';
import { CanDeactivateGuard } from '../../can-deactivate-guard';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { Msg, MsgType } from '../../header/msg.state';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit, CanDeactivateGuard {
  id: any;
  project: Project;
  form: FormGroup;
  formValueAfterLoad = {};
  @Select(AuthState.isAuthorized) authorized$: Observable<boolean>;
  isEditable = false;
  isAuth = false;
  mySubScription = new Subscription();

  constructor(
    private store: Store,
    private route: ActivatedRoute,
    private router: Router,
    private action: Actions,
    private fb: FormBuilder,
    private modal: Modal) {

      this.form = new FormGroup({});
  }

  ngOnInit() {
    this.form = this.fb.group({
      mod: '',
      app_name: '',
      proj_name: '',
      kickoff_date: '',
      qualityFormGroup: this.fb.group({
        project_scope: '',
        tems: '',
        notional_schedule: '',
        quality_budget: '',
        frds: '',
        project_roadmap: '',
        resource_assessment: '',
        cio_approval_date: '',
        publish_schedule: '',
        variance: '' /// can't find database side.
      }),
      sprintFormGroup: this.fb.group({
        sprint0_scheduled_finish_date: '',
        sprint0_actual_finish_date: '',
        iter_scrum1_scheduled_finish_date: '',
        iter_scrum1_actual_finish_date: '',
        code_freeze_scheduled_finish_date: '',
        code_freeze_actual_finish_date: '',
        rmf4_scheduled_finish_date: '',
        rmf4_actual_finish_date: '',
        rmf5_scheduled_finish_date: '',
        rmf5_actual_finish_date: '',
        applicable_milestones: '',
        missed_milestones: '',
        finished_milestones: ''
      }),
      captureFormGroup: this.fb.group({
        reqt_matrix: '',
        user_stories: '',
        product_roadmap: '',
        loe: '',
        initial_schedule: '',
        spend_plan: '',
        cdd: '',
        conops: '',
        isso_mgr: '',
        config_mgr: '',
        scrum_master: '',
        developer1: '',
        developer2: '',
        developer3: '',
        developer4: '',
        developer5: '',
        developer6: ''
      }),
      portfolioFormGroup: this.fb.group({
        pop_start_slin_funded: '',
        cio2_kickoff: '',
        tem_app_pm: '',
        loe_submitted: '',
        govt_approval: '',
        publish_baseline_schedule: '',
        rmf_kickoff: '',
        arr: '',
        code_freeze: '',
        ato: '',
        transitioned_om: '',
        finished_planned: '',
        finished_forecast: '',
        phase_status: '',
        schedule: '',
        resources: '',
        portfolio_budget: '',
        unclassDev: '',
        comment: ''
      }),
      roleFormGroup: this.fb.group({
        project_owner: ''
      })
    });

    this.route.params.subscribe(({ id }) => {
      this.id = id;

      if (id === 'new') {
        this.project = new Project(null, null, null);
        this.isEditable = true;
        return;
      }

      this.store.dispatch(new LoadProject(id));
      this.mySubScription.add(
        this.action.pipe(ofActionSuccessful(LoadProjectSuccess)).subscribe(
          ({ payload }) => {
            this.project = payload;
            this.isEditable = this.project.is_editable;
            this.isAuth && this.isEditable ? this.form.enable() : this.form.disable();
            this.loadDataForm();
            // if ( !this.store.selectSnapshot(AuthState.isAuthorized) ) {
            //   this.form.disable();
            // }
          }
        )
      );

      this.mySubScription.add( this.authorized$.subscribe( (isAuth) => {
        this.isAuth = isAuth;
        isAuth && this.isEditable ? this.form.enable() : this.form.disable();
      }));

    });
  }

  loadDataForm() {
    const project = this.project;
    const dtRegex = /^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/g;
    const convValToDate = function( val ) {
      if ( val != null && val.match(dtRegex) ) {
        const dt: Date = new Date(val.match(dtRegex)[0]);
        return { year: dt.getFullYear(), month: dt.getMonth()+1, day: dt.getDay()+1 };
      }
      return val;
    };

    const cf = function( o, c ) {
      for ( const k in o ) {

        if ( o[k] !== null
          && typeof o[k] === 'object'
          && Object.keys(o[k]).length > 0) {
          c[k] = {};
          cf( o[k], c[k]);
        } else {
          if ( typeof project[k] !== 'undefined' ) {
            c[k] = ColType.getDataType(k) === 'date' ? convValToDate(project[k]) : project[k];
          } else {
            console.warn( k + ':' );
            c[k] = null;
          }
        }
      }
    };

    this.formValueAfterLoad = {};
    cf( this.form.value, this.formValueAfterLoad );
    this.form.reset(this.formValueAfterLoad);

  }

  save() {

    const me = this;

    const convDtObjToString = function( obj ) {
      const month: string = (obj.month<10? '-0':'-') + obj.month;
      const day: string = (obj.day<10?'-0':'-') + obj.day;
      return obj.year + month + day;
    };

    const setVal = function( o ) {

      for ( const k of Object.keys(o) ) {
        console.log( 'k ' + k);
        let isObj = false;
        let isDt = false;

        if ( o[k] != null && typeof o[k] === 'object' ) {
          // make sure its a object with keys and not date object.
          const keys =  Object.keys(o[k]);
          isDt = keys.toString() === 'year,month,day';
          isObj = keys.length > 0 && !isDt;
        }

        if ( isObj ) {
            setVal( o[k] );
        } else {
          if (  ColType.getDataType(k) === 'date' ) {
            me.project[k] = isDt ? convDtObjToString(o[k]) : null;
          } else {
            me.project[k] = o[k];
          }

        }
      }

    };

    setVal(this.form.value);

    console.log( this.project );
    if (this.id === 'new') {
      this.store.dispatch(new AddProject(this.project));
    } else {
      this.store.dispatch(new UpdateProject(this.project));
    }

    const saveSuccess = this.action.pipe(ofActionSuccessful(SaveSuccess));
    saveSuccess.subscribe( (id) => {
      this.form.markAsPristine();
      this.store.dispatch(new Msg(MsgType.success, 'Project is saved.', '/projects'));
    });
    // this.loginSuccess$.subscribe( ( data ) => console.log(data) );


  }

  cancel() {
    this.form.reset(this.formValueAfterLoad);
  }

  canDeactivate() {
    if (this.form.pristine) {
      return true;
    }

    const dialogRef = this.modal
      .confirm()
      .title('')
      .headerClass('modal-header-flee')
      .footerClass('modal-footer-flee')
      .body(`Press OK to abandon changes.`)
      .open();

    return dialogRef.result;

  }

}
