import { State, Action, StateContext, Store, Selector, Select } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { ColModel, InitColModel } from './col.model';
import { UpdColSel, UpdColModel } from './col.action';
import { NgComponentOutlet } from '@angular/common';

@Injectable()

export class ColStateModel {
  columns: ColModel[];
}

@State<ColStateModel>({
  name: 'column',
  defaults: {
    columns: new InitColModel().getColumns()
  }
})

export class ColState {

  @Selector() static getColumns( state ) {
    return state.columns;
  }

  @Action(UpdColSel)
  updColSel(ctx: StateContext<ColStateModel>, action: UpdColSel ) {
    // todo
    const ncol = ctx.getState().columns.slice(0);
    for ( const col of ncol ) {
      col.showx = action.payload[col.position];
    }
    ctx.setState( {
      columns: ncol
    });
  }

  @Action(UpdColModel)
  updColModel(ctx: StateContext<ColStateModel>, action: UpdColModel ) {
    // todo

  }

}


