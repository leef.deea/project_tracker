import { ColModel } from './col.model';

export class InitCol {
  static readonly type = '[Col] Init';
}

export class UpdColSel {
  static readonly type = '[Col] Upd Selection';
  constructor( public payload: boolean[] ) {}
}

export class UpdColModel {
  static readonly type = '[Col] Upd Model';
  constructor( public payload: ColModel ) {}
}
