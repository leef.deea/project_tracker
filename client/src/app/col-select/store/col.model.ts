
export class ColModel {

  type: string;
  column_name: string;
  column_abbr: string;
  spreadsheet: string;
  position: number;
  showx = true;
  width = 100;

  constructor( name, abbr, spreadsheet, width? ) {
    this.column_name = name;
    this.column_abbr = abbr;
    this.spreadsheet = spreadsheet;
    this.width = width;
    this.type = name === spreadsheet ? 'p' : 'c';
  }
}

export class InitColModel {

  getColumns(): ColModel[] {

    const columns: ColModel[] = [];

    columns.push( new ColModel('Detail', 'Detail', 'Detail') );
    columns.push( new ColModel('Id', 'id', 'Detail', 75) );
    columns.push( new ColModel('MOD', 'mod', 'Detail', 75) );
    columns.push( new ColModel('Application Name', 'app_name', 'Detail', 300) );
    columns.push( new ColModel('Project Name', 'proj_name', 'Detail') );
    columns.push( new ColModel('Project Manager', 'project_mgr', 'Detail') );
    columns.push( new ColModel('Kickoff Date', 'kickoff_date', 'Detail') );

    columns.push( new ColModel('Quality', 'Quality', 'Quality') );
    columns.push( new ColModel('Project Scope', 'project_scope', 'Quality') );
    columns.push( new ColModel('TEMS', 'tems', 'Quality') );
    columns.push( new ColModel('Notional Schedule', 'notional_schedule', 'Quality') );
    columns.push( new ColModel('FRDS', 'frds', 'Quality') );
    columns.push( new ColModel('Project Roadmap', 'project_roadmap', 'Quality') );
    columns.push( new ColModel('Resource Assessment', 'resource_assessment', 'Quality') );
    columns.push( new ColModel('CIO Approval Date', 'cio_approval_date', 'Quality') );
    columns.push( new ColModel('Publish Schedule', 'publish_schedule', 'Quality') );

    // columns.push( new ColModel('Created By', 'created_by', 'Quality') );
    // columns.push( new ColModel('Created Date', 'created_date', 'Quality') );
    // columns.push( new ColModel('Updated By', 'updated_by', 'Quality') );
    // columns.push( new ColModel('Updated Date', 'updated_date', 'Quality') );

    columns.push( new ColModel('Sprint', 'Sprint', 'Sprint') );

    columns.push( new ColModel('Sprint0 Scheduled', 'sprint0_scheduled_finish_date', 'Sprint') );
    columns.push( new ColModel('Sprint0 Finish Date', 'sprint0_finish_date', 'Sprint') );
    columns.push( new ColModel('Iteration Scrum1 Scheduled Finish Date', 'iter_scrum1_scheduled_finish_date', 'Sprint') );
    columns.push( new ColModel('Iteration Scrum1 Finish Date', 'iter_scrum1_finish_date', 'Sprint') );
    columns.push( new ColModel('Code Freeze Scheduled Finish Date', 'code_freeze_scheduled_finish_date', 'Sprint') );
    columns.push( new ColModel('Code Freeze Finish Date', 'code_freeze_finish_date', 'Sprint') );
    columns.push( new ColModel('RMF4 Scheduled Finish Date', 'rmf4_scheduled_finish_date', 'Sprint') );
    columns.push( new ColModel('RMF4 Finish Date', 'rmf4_finish_date', 'Sprint') );
    columns.push( new ColModel('Applicable Milestones', 'applicable_milestones', 'Sprint') );
    columns.push( new ColModel('Missed Milestones', 'missed_milestones', 'Sprint') );
    columns.push( new ColModel('Finished Milestones', 'finished_milestones', 'Sprint') );

    // columns.push( new ColModel('Created By', 'created_by', 'Sprint') );
    // columns.push( new ColModel('Created Date', 'created_date', 'Sprint') );
    // columns.push( new ColModel('Updated by', 'updated_by', 'Sprint') );
    // columns.push( new ColModel('Updated Date', 'updated_date', 'Sprint') );


    columns.push( new ColModel('Capture', 'Capture', 'Capture') );
    columns.push( new ColModel('Request Matrix', 'reqt_matrix', 'Capture') );
    columns.push( new ColModel('User Stories', 'user_stories', 'Capture') );
    columns.push( new ColModel('Pri', 'pri', 'Capture') );
    columns.push( new ColModel('Amp Lead', 'amp_lead', 'Capture') );
    columns.push( new ColModel('Gold Team Request Lead', 'gold_team_reqt_lead', 'Capture') );
    columns.push( new ColModel('Product Roadmap', 'product_roadmap', 'Capture') );
    columns.push( new ColModel('Loe', 'loe', 'Capture') );
    columns.push( new ColModel('Initial Schedule', 'initial_schedule', 'Capture') );
    columns.push( new ColModel('Spend Plan', 'spend_plan', 'Capture') );
    columns.push( new ColModel('CDD', 'cdd', 'Capture') );
    columns.push( new ColModel('CONOPS', 'conops', 'Capture') );
    columns.push( new ColModel('ISSO Manager', 'isso_mgr', 'Capture') );
    columns.push( new ColModel('Config Manager', 'config_mgr', 'Capture') );
    columns.push( new ColModel('Scrum Master', 'scrum_master', 'Capture') );


    // columns.push( new ColModel('Comment', 'Comment', 'Comment') );

    // columns.push( new ColModel('Scope', 'scope', 'Comment') );
    // columns.push( new ColModel('Portfolio ID', 'portfolio_id', 'Comment') );

    // columns.push( new ColModel('Dev', 'Dev', 'Dev') );

    // columns.push( new ColModel('First Name', 'first_name', 'Dev') );
    // columns.push( new ColModel('Last Name', 'last_name', 'Dev') );
    // columns.push( new ColModel('Allocation Percent', 'allocation_percent', 'Dev') );
    // columns.push( new ColModel('Capture ID', 'capture_id', 'Dev') );

    columns.push( new ColModel('Portfolio', 'Portfolio', 'Portfolio') );

    columns.push( new ColModel('Fiscal Year', 'fiscal_year', 'Portfolio') );
    columns.push( new ColModel('Government POC', 'govt_poc', 'Portfolio') );
    columns.push( new ColModel('Pop Start SLIN Funded', 'pop_start_slin_funded', 'Portfolio') );
    columns.push( new ColModel('CIO2 Kickoff', 'cio2_kickoff', 'Portfolio') );
    columns.push( new ColModel('TEM APP PM', 'tem_app_pm', 'Portfolio') );
    columns.push( new ColModel('LOE Submitted', 'loe_submitted', 'Portfolio') );
    columns.push( new ColModel('Government Approval', 'govt_approval', 'Portfolio') );
    columns.push( new ColModel('Publish Baseline Schedule', 'publish_baseline_schedule', 'Portfolio') );
    columns.push( new ColModel('RMF Kickoff', 'rmf_kickoff', 'Portfolio') );
    columns.push( new ColModel('ARR', 'arr', 'Portfolio') );
    columns.push( new ColModel('Code Freeze', 'code_freeze', 'Portfolio') );
    columns.push( new ColModel('ATO', 'ato', 'Portfolio') );
    columns.push( new ColModel('Transitioned OM', 'transitioned_om', 'Portfolio') );
    columns.push( new ColModel('Finished Planned', 'finished_planned', 'Portfolio') );
    columns.push( new ColModel('Finished Forecast', 'finished_forecase', 'Portfolio') );
    columns.push( new ColModel('Status', 'status', 'Portfolio') );
    columns.push( new ColModel('Resources', 'resources', 'Portfolio') );
    columns.push( new ColModel('Schedule', 'schedule', 'Portfolio') );
    columns.push( new ColModel('Budget', 'budget', 'Portfolio') );
    columns.push( new ColModel('Unclass Dev', 'unclass_dev', 'Portfolio') );
    // columns.push( new ColModel('Project Details ID', 'proj_details_id', 'Portfolio') );
    // columns.push( new ColModel('Capture ID', 'capture_id', 'Portfolio') );
    // columns.push( new ColModel('SLIN ID', 'slin_id', 'Portfolio') );

    // columns.push( new ColModel('Roles', 'Roles', 'Roles') );

    // columns.push( new ColModel('Role', 'role', 'Roles') );
    // columns.push( new ColModel('Users ID', 'users_id', 'Roles') );

    // columns.push( new ColModel('SLIN', 'SLIN', 'SLIN') );

    // columns.push( new ColModel('Number', 'number', 'SLIN') );
    // columns.push( new ColModel('Description', 'description', 'SLIN') );

    columns.push( new ColModel('Audit', 'Audit', 'Audit') );
    columns.push( new ColModel('Created By', 'created_by', 'Audit', 150) );
    columns.push( new ColModel('Created Date', 'created_date', 'Audit') );
    columns.push( new ColModel('Updated By', 'updated_by', 'Audit', 150) );
    columns.push( new ColModel('Updated Date', 'updated_date', 'Audit') );

    for ( let i = 0; i < columns.length; i++ ) {
      columns[i].position = i;
    }

    return columns;

  }
}

