import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { ColState } from './store/col.state';
import { ColModel } from './store/col.model';
import { UpdColSel } from './store/col.action';
import { FormGroup, FormArray, FormControl } from '@angular/forms';

@Component({
  selector: 'app-col-select',
  templateUrl: './col-select.component.html',
  styleUrls: ['./col-select.component.css']
})
export class ColSelectComponent implements OnInit {

  colVisGroup: FormGroup;
  cols: ColModel[];
  constructor(private store: Store) { }

  ngOnInit() {
    this.cols = this.store.selectSnapshot( ColState.getColumns );
    console.log( this.cols );
    const controls = [];
    for ( const col of this.cols ) {
      controls.push(new FormControl(col.showx));
    }
    this.colVisGroup = new FormGroup({
      'columns': new FormArray(controls)
    });
  }

  apply() {
    this.store.dispatch( new UpdColSel(this.colVisGroup.value.columns) );
  }
  change( idx ) {

    const updCol = this.cols.filter( (col) => col.column_abbr === idx )[0];
    const val = this.colVisGroup.value.columns[updCol.position];
    const spdCol = this.cols.filter( (col) => col.spreadsheet === updCol.spreadsheet);
    if ( updCol.type === 'p' ) {
      spdCol.map(
        (col) => col.showx = val
      );
    } else {
      let totalSelected = 0;
      updCol.showx = val;
      spdCol.filter((col) => col.type !== 'p').forEach( ( col ) => totalSelected += col.showx ? 1 : 0);
      const headVal = totalSelected === spdCol.length - 1;
      const headCol = spdCol.filter((col) => col.type === 'p')[0];
      headCol.showx = headVal;
    }
    this.colVisGroup.controls.columns.setValue( this.cols.map( c => c.showx ) );
    this.store.dispatch( new UpdColSel(this.colVisGroup.value.columns) );

  }
}
