export abstract class Audit {
  public created_by: string;
  public created_date: Date;
  public updated_by: string;
  public updated_date: Date;
}
