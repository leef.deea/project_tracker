--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Ubuntu 10.5-0ubuntu0.18.04)
-- Dumped by pg_dump version 10.5 (Ubuntu 10.5-0ubuntu0.18.04)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: dee; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA dee;


ALTER SCHEMA dee OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: updated_date_column(); Type: FUNCTION; Schema: dee; Owner: postgres
--

CREATE FUNCTION dee.updated_date_column() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    NEW.updated_date = current_timestamp;
    RETURN NEW; 
END;
$$;


ALTER FUNCTION dee.updated_date_column() OWNER TO postgres;

--
-- Name: capture_id_seq; Type: SEQUENCE; Schema: dee; Owner: postgres
--

CREATE SEQUENCE dee.capture_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dee.capture_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: capture; Type: TABLE; Schema: dee; Owner: postgres
--

CREATE TABLE dee.capture (
    id bigint DEFAULT nextval('dee.capture_id_seq'::regclass) NOT NULL,
    pri integer,
    amp_lead character varying(50),
    gold_team_reqt_lead character varying(50),
    reqt_matrix character varying(50),
    user_stories character varying(50),
    product_roadmap character varying(50),
    loe character varying(50),
    initial_schedule character varying(50),
    spend_plan character varying(50),
    cdd character varying(50),
    conops character varying(50),
    isso_mgr character varying(50),
    config_mgr character varying(50),
    scrum_master character varying(50),
    created_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    updated_date timestamp without time zone DEFAULT now() NOT NULL,
    proj_details_id integer NOT NULL
);


ALTER TABLE dee.capture OWNER TO postgres;

--
-- Name: comment_id_seq; Type: SEQUENCE; Schema: dee; Owner: postgres
--

CREATE SEQUENCE dee.comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dee.comment_id_seq OWNER TO postgres;

--
-- Name: comment; Type: TABLE; Schema: dee; Owner: postgres
--

CREATE TABLE dee.comment (
    id bigint DEFAULT nextval('dee.comment_id_seq'::regclass) NOT NULL,
    scope character varying(50),
    status character varying(300),
    created_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    updated_date timestamp without time zone DEFAULT now() NOT NULL,
    portfolio_id integer NOT NULL,
    proj_details_id integer NOT NULL
);


ALTER TABLE dee.comment OWNER TO postgres;

--
-- Name: dev; Type: TABLE; Schema: dee; Owner: postgres
--

CREATE TABLE dee.dev (
    id bigint NOT NULL,
    first_name character varying(50),
    last_name character varying(50),
    allocation_percent numeric(6,3),
    created_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    updated_date timestamp without time zone DEFAULT now() NOT NULL,
    capture_id integer,
    proj_details_id integer NOT NULL
);


ALTER TABLE dee.dev OWNER TO postgres;

--
-- Name: dev_id_seq; Type: SEQUENCE; Schema: dee; Owner: postgres
--

CREATE SEQUENCE dee.dev_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dee.dev_id_seq OWNER TO postgres;

--
-- Name: dev_id_seq1; Type: SEQUENCE; Schema: dee; Owner: postgres
--

CREATE SEQUENCE dee.dev_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dee.dev_id_seq1 OWNER TO postgres;

--
-- Name: dev_id_seq1; Type: SEQUENCE OWNED BY; Schema: dee; Owner: postgres
--

ALTER SEQUENCE dee.dev_id_seq1 OWNED BY dee.dev.id;


--
-- Name: portfolio_id_seq; Type: SEQUENCE; Schema: dee; Owner: postgres
--

CREATE SEQUENCE dee.portfolio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dee.portfolio_id_seq OWNER TO postgres;

--
-- Name: portfolio; Type: TABLE; Schema: dee; Owner: postgres
--

CREATE TABLE dee.portfolio (
    id bigint DEFAULT nextval('dee.portfolio_id_seq'::regclass) NOT NULL,
    fiscal_year integer,
    govt_poc character varying(50),
    pop_start_slin_funded date,
    cio2_kickoff date,
    tem_app_pm date,
    loe_submitted date,
    govt_approval date,
    publish_baseline_schedule date,
    rmf_kickoff date,
    arr date,
    code_freeze date,
    ato date,
    transitioned_om date,
    finished_planned date,
    finished_forecast date,
    status character varying(50),
    schedule character varying(50),
    resources character varying(50),
    budget character varying(50),
    unclass_dev character varying(50),
    created_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    updated_date timestamp without time zone DEFAULT now() NOT NULL,
    proj_details_id integer NOT NULL,
    capture_id integer NOT NULL,
    slin_id integer NULL
);


ALTER TABLE dee.portfolio OWNER TO postgres;

--
-- Name: proj_details_id_seq; Type: SEQUENCE; Schema: dee; Owner: postgres
--

CREATE SEQUENCE dee.proj_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dee.proj_details_id_seq OWNER TO postgres;

--
-- Name: proj_details; Type: TABLE; Schema: dee; Owner: postgres
--

CREATE TABLE dee.proj_details (
    id integer DEFAULT nextval('dee.proj_details_id_seq'::regclass) NOT NULL,
    mod character varying(50),
    app_name character varying(50),
    proj_name character varying(50),
    project_mgr character varying(50),
    kickoff_date date,
    created_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    updated_date timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE dee.proj_details OWNER TO postgres;

--
-- Name: quality_id_seq; Type: SEQUENCE; Schema: dee; Owner: postgres
--

CREATE SEQUENCE dee.quality_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dee.quality_id_seq OWNER TO postgres;

--
-- Name: quality; Type: TABLE; Schema: dee; Owner: postgres
--

CREATE TABLE dee.quality (
    id bigint DEFAULT nextval('dee.quality_id_seq'::regclass) NOT NULL,
    project_scope character varying(50),
    tems character varying(100),
    notional_schedule date,
    budget character varying(50),
    frds character varying(50),
    project_roadmap character varying(50),
    resource_assessment character varying(50),
    cio_approval_date character varying(50),
    publish_schedule date,
    created_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    updated_date timestamp without time zone DEFAULT now() NOT NULL,
    proj_details_id integer NOT NULL
);


ALTER TABLE dee.quality OWNER TO postgres;

--
-- Name: roles_id_seqq; Type: SEQUENCE; Schema: dee; Owner: postgres
--

CREATE SEQUENCE dee.roles_id_seqq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dee.roles_id_seqq OWNER TO postgres;

--
-- Name: roles; Type: TABLE; Schema: dee; Owner: postgres
--

CREATE TABLE dee.roles (
    id bigint DEFAULT nextval('dee.roles_id_seqq'::regclass) NOT NULL,
    role character varying(50),
    created_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    updated_date timestamp without time zone DEFAULT now() NOT NULL,
    users_id integer NOT NULL
);


ALTER TABLE dee.roles OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: dee; Owner: postgres
--

CREATE SEQUENCE dee.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dee.roles_id_seq OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: dee; Owner: postgres
--

ALTER SEQUENCE dee.roles_id_seq OWNED BY dee.roles.id;


--
-- Name: slin_id_seq; Type: SEQUENCE; Schema: dee; Owner: postgres
--

CREATE SEQUENCE dee.slin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dee.slin_id_seq OWNER TO postgres;

--
-- Name: slin; Type: TABLE; Schema: dee; Owner: postgres
--

CREATE TABLE dee.slin (
    id bigint DEFAULT nextval('dee.slin_id_seq'::regclass) NOT NULL,
    number character varying(50),
    description character varying(50),
    created_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    updated_date timestamp without time zone DEFAULT now() NOT NULL,
    proj_details_id integer NOT NULL
);


ALTER TABLE dee.slin OWNER TO postgres;

--
-- Name: sprint_id_seq; Type: SEQUENCE; Schema: dee; Owner: postgres
--

CREATE SEQUENCE dee.sprint_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dee.sprint_id_seq OWNER TO postgres;

--
-- Name: sprint; Type: TABLE; Schema: dee; Owner: postgres
--

CREATE TABLE dee.sprint (
    id bigint DEFAULT nextval('dee.sprint_id_seq'::regclass) NOT NULL,
    sprint0_scheduled_finish_date date,
    sprint0_actual_finish_date date,
    iter_scrum1_scheduled_finish_date date,
    iter_scrum1_actual_finish_date date,
    code_freeze_scheduled_finish_date date,
    code_freeze_actual_finish_date date,
    rmf4_scheduled_finish_date date,
    rmf4_actual_finish_date date,
    applicable_milestones character varying(300),
    missed_milestones character varying(300),
    finished_milestones character varying(300),
    created_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    updated_date timestamp without time zone DEFAULT now() NOT NULL,
    proj_details_id integer,
    rm5_scheduled_finish_date date,
    rm5_actual_finish_date date
);


ALTER TABLE dee.sprint OWNER TO postgres;

--
-- Name: table_references; Type: TABLE; Schema: dee; Owner: postgres
--

CREATE TABLE dee.table_references (
    id integer NOT NULL,
    spreadsheet_actual_column_name character varying(50),
    spreadsheet_abbrev_column_name character varying(50),
    spreadsheet_actual_name character varying(50),
    created_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    updated_date timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE dee.table_references OWNER TO postgres;

--
-- Name: table_references_id_seq; Type: SEQUENCE; Schema: dee; Owner: postgres
--

CREATE SEQUENCE dee.table_references_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dee.table_references_id_seq OWNER TO postgres;

--
-- Name: table_references_id_seq; Type: SEQUENCE OWNED BY; Schema: dee; Owner: postgres
--

ALTER SEQUENCE dee.table_references_id_seq OWNED BY dee.table_references.id;


--
-- Name: users_id_seqq; Type: SEQUENCE; Schema: dee; Owner: postgres
--

CREATE SEQUENCE dee.users_id_seqq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dee.users_id_seqq OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: dee; Owner: postgres
--

CREATE TABLE dee.users (
    id bigint DEFAULT nextval('dee.users_id_seqq'::regclass) NOT NULL,
    first_name character varying(50) NOT NULL,
    last_name character varying(50) NOT NULL,
    user_name character varying(50) NOT NULL,
    email_address character varying(50) NOT NULL,
    password_hash text NOT NULL,
    created_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_by character varying(50) DEFAULT CURRENT_USER NOT NULL,
    updated_date timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE dee.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: dee; Owner: postgres
--

CREATE SEQUENCE dee.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dee.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: dee; Owner: postgres
--

ALTER SEQUENCE dee.users_id_seq OWNED BY dee.users.id;


--
-- Name: dev id; Type: DEFAULT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.dev ALTER COLUMN id SET DEFAULT nextval('dee.dev_id_seq1'::regclass);


--
-- Name: table_references id; Type: DEFAULT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.table_references ALTER COLUMN id SET DEFAULT nextval('dee.table_references_id_seq'::regclass);


--
-- Data for Name: capture; Type: TABLE DATA; Schema: dee; Owner: postgres
--

COPY dee.capture (id, pri, amp_lead, gold_team_reqt_lead, reqt_matrix, user_stories, product_roadmap, loe, initial_schedule, spend_plan, cdd, conops, isso_mgr, config_mgr, scrum_master, created_by, created_date, updated_by, updated_date, proj_details_id) FROM stdin;
4	1	Ricky Downey Jr	Oscar Meyer	Kurt Angle	Matlock		Srini Madala	Randy House						Vanna Dela Cruz	postgres	2018-08-27 14:51:05.387592	postgres	2018-08-27 14:51:05.387592	5
5	4	Gus Averill	Joyce Meyer	Kurt Krieger	Chris Whitlock	\N	Srini Madala	Randy House	\N	\N	\N	\N	\N	Donna Desiderio	postgres	2018-08-27 14:53:16.260783	postgres	2018-08-27 14:53:16.260783	4
\.


--
-- Data for Name: comment; Type: TABLE DATA; Schema: dee; Owner: postgres
--

COPY dee.comment (id, scope, status, created_by, created_date, updated_by, updated_date, portfolio_id, proj_details_id) FROM stdin;
6	\N	I have no idea	postgres	2018-08-27 15:06:06.244631	postgres	2018-08-27 15:06:06.244631	2	4
4	Something begat someting	8/10 Updating user stories in progress,  draft CONOPs in progress, LOE in process. \nDarren H. need to confirm funding line for CRATE - Request Tracker	postgres	2018-08-27 15:04:50.03637	postgres	2018-08-27 15:06:35.900423	1	5
\.


--
-- Data for Name: dev; Type: TABLE DATA; Schema: dee; Owner: postgres
--

COPY dee.dev (id, first_name, last_name, allocation_percent, created_by, created_date, updated_by, updated_date, capture_id, proj_details_id) FROM stdin;
6	postgres	hall	0.500	postgres	2018-08-27 14:40:23.607893	postgres	2018-08-27 14:40:23.607893	\N	2
7	ricky	bobby	-1.000	postgres	2018-08-27 14:44:18.50171	postgres	2018-08-27 14:44:18.50171	\N	3
\.


--
-- Data for Name: portfolio; Type: TABLE DATA; Schema: dee; Owner: postgres
--

COPY dee.portfolio (id, fiscal_year, govt_poc, pop_start_slin_funded, cio2_kickoff, tem_app_pm, loe_submitted, govt_approval, publish_baseline_schedule, rmf_kickoff, arr, code_freeze, ato, transitioned_om, finished_planned, finished_forecast, status, schedule, resources, budget, unclass_dev, created_by, created_date, updated_by, updated_date, proj_details_id, capture_id, slin_id) FROM stdin;
1	17	James Barrow	2018-03-27	2011-07-14	2014-04-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Plan	na	na	G	\N	postgres	2018-08-27 12:30:39.341097	postgres	2018-08-27 15:10:06.811155	2	4	1
2	18	Clyde Drexler	2020-01-22	2009-06-27	2011-11-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Plan	na	na	R	\N	postgres	2018-08-27 12:30:39.341097	postgres	2018-08-27 15:10:13.555529	3	5	2
\.


--
-- Data for Name: proj_details; Type: TABLE DATA; Schema: dee; Owner: postgres
--

COPY dee.proj_details (id, mod, app_name, proj_name, project_mgr, kickoff_date, created_by, created_date, updated_by, updated_date) FROM stdin;
2	2	REQUEST TRACKER	ADX DEV ENVIRONMENT	Josh Doss	2018-08-12	hallp	2018-08-22 12:17:03.030272	hallp	2018-08-22 12:17:03.030272
3	1	ROMEO	CHROME DEV ENVIRONMENT	Jason Lee	2018-07-30	hallp	2018-08-22 12:17:56.266644	hallp	2018-08-22 12:17:56.266644
4	4	PIT	CHROME DEV ENVIRONMENT	Jason Lee	2018-07-30	hallp	2018-08-22 12:18:36.394055	hallp	2018-08-22 12:20:16.988835
5	6	RUBY	CHROME DEV ENVIRONMENT	Ryan Donnels	2017-11-30	postgres	2018-08-23 08:47:59.125315	postgres	2018-08-23 08:47:59.125315
\.


--
-- Data for Name: quality; Type: TABLE DATA; Schema: dee; Owner: postgres
--

COPY dee.quality (id, project_scope, tems, notional_schedule, budget, frds, project_roadmap, resource_assessment, cio_approval_date, publish_schedule, created_by, created_date, updated_by, updated_date, proj_details_id) FROM stdin;
0	yes	yes every 2 weeks	2018-08-22	yes 2 SLINs	In Progress	No	Yes	2018-08-21	2018-08-22	hallp	2011-05-16 15:36:38	hallp	2018-08-27 15:11:52.37527	3
1	NO	yes every 2 weeks	2018-08-22	yes,3 SLINs	In Progress	yes	Yes	2018-08-21	2018-08-22	hallp	2018-08-22 12:15:01.087328	hallp	2018-08-27 15:11:52.384337	2
2	yes	n/a	2018-04-15	yes, 5 SLINs	in progress	no	yes	no	1981-03-12	postgres	2018-08-27 11:43:40.859284	postgres	2018-08-27 15:11:52.385805	5
3	no	07/31/2018 TEM w/customer 08/01/2018 TEM w/customer	2018-04-15	no, 5 SLINs	in progress	no	yes	07/11/1991	1981-03-12	postgres	2018-08-27 11:46:36.523673	postgres	2018-08-27 15:11:52.387153	4
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: dee; Owner: postgres
--

COPY dee.roles (id, role, created_by, created_date, updated_by, updated_date, users_id) FROM stdin;
\.


--
-- Data for Name: slin; Type: TABLE DATA; Schema: dee; Owner: postgres
--

COPY dee.slin (id, number, description, created_by, created_date, updated_by, updated_date, proj_details_id) FROM stdin;
1	001AG, 0001BV	Project Apples	postgres	2018-08-27 11:49:03.733077	postgres	2018-08-27 15:09:04.679821	3
2	002PP, 010AD, 11BC	Project Oranges	postgres	2018-08-27 11:49:44.450869	postgres	2018-08-27 15:09:04.688964	4
\.


--
-- Data for Name: sprint; Type: TABLE DATA; Schema: dee; Owner: postgres
--

COPY dee.sprint (id, sprint0_scheduled_finish_date, sprint0_actual_finish_date, iter_scrum1_scheduled_finish_date, iter_scrum1_actual_finish_date, code_freeze_scheduled_finish_date, code_freeze_actual_finish_date, rmf4_scheduled_finish_date, rmf4_actual_finish_date, applicable_milestones, missed_milestones, finished_milestones, created_by, created_date, updated_by, updated_date, proj_details_id, rm5_scheduled_finish_date, rm5_actual_finish_date) FROM stdin;
1	2001-04-01	2001-04-05	2012-07-08	2012-07-08	2012-07-10	2012-07-15	\N	\N	we finished	missed on time scrum dates	n/a	postgres	2018-08-27 15:36:14.987377	postgres	2018-08-27 15:36:14.987377	4	\N	\N
2	2018-09-22	2018-10-21	2020-12-31	2020-12-30	2014-11-03	2014-12-15	\N	\N	huge uh oh	no one got hurt	idk	postgres	2018-08-27 15:38:22.116874	postgres	2018-08-27 15:38:22.116874	3	\N	\N
\.


--
-- Data for Name: table_references; Type: TABLE DATA; Schema: dee; Owner: postgres
--

COPY dee.table_references (id, spreadsheet_actual_column_name, spreadsheet_abbrev_column_name, spreadsheet_actual_name, created_by, created_date, updated_by, updated_date) FROM stdin;
1	pri	pri	curt	postgres	2018-08-27 08:30:09.907448	postgres	2018-08-27 08:30:09.907448
2	amp lead	amp_lead	curt	postgres	2018-08-27 08:30:09.907448	postgres	2018-08-27 08:30:09.907448
3	gold team reqt lead	gold_team_reqt_lead	curt	postgres	2018-08-27 08:30:09.907448	postgres	2018-08-27 08:30:09.907448
4	user stories	user_stories	curt	postgres	2018-08-27 08:30:09.907448	postgres	2018-08-27 08:30:09.907448
5	product roadmap	product_roadmap	curt	postgres	2018-08-27 08:30:09.907448	postgres	2018-08-27 08:30:09.907448
6	loe	loe	curt	postgres	2018-08-27 08:30:09.907448	postgres	2018-08-27 08:30:09.907448
7	initial schedule	initial_schedule	curt	postgres	2018-08-27 08:30:09.907448	postgres	2018-08-27 08:30:09.907448
8	spend plan	spend_plan	curt	postgres	2018-08-27 08:30:09.907448	postgres	2018-08-27 08:30:09.907448
9	cdd	cdd	curt	postgres	2018-08-27 08:30:09.907448	postgres	2018-08-27 08:30:09.907448
10	CONOPS	conops	curt	postgres	2018-08-27 08:30:09.907448	postgres	2018-08-27 08:30:09.907448
11	isso mgr	isso_mgr	curt	postgres	2018-08-27 08:30:09.907448	postgres	2018-08-27 08:30:09.907448
12	config mgr	config_mgr	curt	postgres	2018-08-27 08:30:09.907448	postgres	2018-08-27 08:30:09.907448
13	scrum master	scrum_master	curt	postgres	2018-08-27 08:30:09.907448	postgres	2018-08-27 08:30:09.907448
14	comments	scope	mina	postgres	2018-08-27 08:34:02.250573	postgres	2018-08-27 08:34:02.250573
15	comments	status	mina	postgres	2018-08-27 08:34:02.250573	postgres	2018-08-27 08:34:02.250573
16	dev	first_name	curt	postgres	2018-08-27 08:36:29.319577	postgres	2018-08-27 08:36:29.319577
17	dev	last_name	curt	postgres	2018-08-27 08:36:29.319577	postgres	2018-08-27 08:36:29.319577
18	dev	allocation_percent	curt	postgres	2018-08-27 08:36:29.319577	postgres	2018-08-27 08:36:29.319577
19	fy	fiscal_year	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
20	gpoc	govt_poc	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
21	PoP start SLIN funded	pop_start_slin_funded	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
22	cio2 kickoff	cio2_kickoff	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
23	TEM App PM	tem_app_pm	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
24	Plan/LOE Submitted	loe_submitted	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
25	Govt Approval	govt_approval	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
26	Publish Baseline Scheudle	publish_baseline_schedule	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
27	RMF Kickoff	rmf_kickoff	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
28	ARR	arr	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
29	Code Freeze	code_freeze	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
30	ATO	ato	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
31	Transitioned to O&M	transitioned_om	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
32	Finish Planned	finished_planned	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
33	Finish Forecast	finished_forecast	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
34	Phase/Status	status	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
35	Schedule	schedule	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
36	Resources	resources	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
37	Budget	budget	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
38	Unclass Dev	unclass_dev	mina	postgres	2018-08-27 08:47:19.82648	postgres	2018-08-27 08:47:19.82648
39	MOD	mod	curt, mina, ssi, sla	postgres	2018-08-27 08:51:06.883505	postgres	2018-08-27 08:51:06.883505
40	app	app_name	curt, mina, ssi, sla	postgres	2018-08-27 08:51:06.883505	postgres	2018-08-27 08:51:06.883505
41	project	proj_name	curt, mina, ssi, sla	postgres	2018-08-27 08:51:06.883505	postgres	2018-08-27 08:51:06.883505
42	PM	project_mgr	curt, mina, ssi, ssa	postgres	2018-08-27 08:51:06.883505	postgres	2018-08-27 08:51:06.883505
43	CIO-2 Kickoff Date	kickoff_date	curt, mina, ssi, sla	postgres	2018-08-27 08:51:06.883505	postgres	2018-08-27 08:51:06.883505
44	project scope	project_scope	sla 6_coa and_epm	postgres	2018-08-27 08:57:40.43356	postgres	2018-08-27 08:57:40.43356
45	TEMs	tems	sla 6_coa and_epm	postgres	2018-08-27 08:57:40.43356	postgres	2018-08-27 08:57:40.43356
46	notional schedule	notional_schedule	sla 6_coa and_epm	postgres	2018-08-27 08:57:40.43356	postgres	2018-08-27 08:57:40.43356
47	budget	budget	sla 6_coa and_epm	postgres	2018-08-27 08:57:40.43356	postgres	2018-08-27 08:57:40.43356
48	FRDSs	frds	sla 6_coa and_epm	postgres	2018-08-27 08:57:40.43356	postgres	2018-08-27 08:57:40.43356
49	project roadmap (DODAF Arch)	project_roadmap	sla 6_coa and_epm	postgres	2018-08-27 08:57:40.43356	postgres	2018-08-27 08:57:40.43356
50	resource assessment	resource_assessment	sla 6_coa and_epm	postgres	2018-08-27 08:57:40.43356	postgres	2018-08-27 08:57:40.43356
51	CIO approval (Date)	cio_approval_date	sla 6_coa and_epm	postgres	2018-08-27 08:57:40.43356	postgres	2018-08-27 08:57:40.43356
52	publish baseline schedule EPM 15 days CIO approval	publish_schedule	sla 6_coa and_epm	postgres	2018-08-27 08:57:40.43356	postgres	2018-08-27 08:57:40.43356
53	slin	number	mina	postgres	2018-08-27 09:09:40.842549	postgres	2018-08-27 09:09:40.842549
54	slin	description	mina	postgres	2018-08-27 09:09:40.842549	postgres	2018-08-27 09:09:40.842549
66	Sprint 0 Completed (Scheduled Date)	sprint0_scheduled_finish_date	sla	postgres	2018-08-27 09:23:47.799836	postgres	2018-08-27 09:23:47.799836
67	Sprint 0 Completion (Actual Date)	sprint0_actual_finish_date	sla	postgres	2018-08-27 09:23:47.799836	postgres	2018-08-27 09:23:47.799836
68	Iteration/Scrum 1 complete (Scheduled Date)	iter_scrum1_scheduled_finish_date	sla	postgres	2018-08-27 09:23:47.799836	postgres	2018-08-27 09:23:47.799836
69	Scrum 1 actual date of completion	iter_scrum1_actual_finish_date	sla	postgres	2018-08-27 09:23:47.799836	postgres	2018-08-27 09:23:47.799836
70	100% development completed	code_freeze_scheduled_finish_date	sla	postgres	2018-08-27 09:23:47.799836	postgres	2018-08-27 09:23:47.799836
71	code freeze actual date of completion	code_freeze_actual_finish_date	sla	postgres	2018-08-27 09:23:47.799836	postgres	2018-08-27 09:23:47.799836
72	RMF Step 4 Completed (ATO)	rmf4_scheduled_finish_date	sla	postgres	2018-08-27 09:23:47.799836	postgres	2018-08-27 09:23:47.799836
73	RMF Step 4 Actual Date of Completion	rmf4_actual_finish_date	sla	postgres	2018-08-27 09:23:47.799836	postgres	2018-08-27 09:23:47.799836
74	RMF Step 5 Scheduled Completion Date	rmf5_scheduled_finish_date	sla	postgres	2018-08-27 09:23:47.799836	postgres	2018-08-27 09:23:47.799836
75	RMF Step 5 Actul Completion Date	rmf5_actual_finish_date	sla	postgres	2018-08-27 09:23:47.799836	postgres	2018-08-27 09:23:47.799836
76	applicable milestones	applicable_milestones	sla	postgres	2018-08-27 09:23:47.799836	postgres	2018-08-27 09:23:47.799836
77	missed milestones	missed_milestones	sla	postgres	2018-08-27 09:23:47.799836	postgres	2018-08-27 09:23:47.799836
78	milestones complete	finished_milestones	sla	postgres	2018-08-27 09:23:47.799836	postgres	2018-08-27 09:23:47.799836
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: dee; Owner: postgres
--

COPY dee.users (id, first_name, last_name, user_name, email_address, password_hash, created_by, created_date, updated_by, updated_date) FROM stdin;
\.


--
-- Name: capture_id_seq; Type: SEQUENCE SET; Schema: dee; Owner: postgres
--

SELECT pg_catalog.setval('dee.capture_id_seq', 5, true);


--
-- Name: comment_id_seq; Type: SEQUENCE SET; Schema: dee; Owner: postgres
--

SELECT pg_catalog.setval('dee.comment_id_seq', 6, true);


--
-- Name: dev_id_seq; Type: SEQUENCE SET; Schema: dee; Owner: postgres
--

SELECT pg_catalog.setval('dee.dev_id_seq', 4, true);


--
-- Name: dev_id_seq1; Type: SEQUENCE SET; Schema: dee; Owner: postgres
--

SELECT pg_catalog.setval('dee.dev_id_seq1', 7, true);


--
-- Name: portfolio_id_seq; Type: SEQUENCE SET; Schema: dee; Owner: postgres
--

SELECT pg_catalog.setval('dee.portfolio_id_seq', 2, true);


--
-- Name: proj_details_id_seq; Type: SEQUENCE SET; Schema: dee; Owner: postgres
--

SELECT pg_catalog.setval('dee.proj_details_id_seq', 5, true);


--
-- Name: quality_id_seq; Type: SEQUENCE SET; Schema: dee; Owner: postgres
--

SELECT pg_catalog.setval('dee.quality_id_seq', 3, true);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: dee; Owner: postgres
--

SELECT pg_catalog.setval('dee.roles_id_seq', 1, false);


--
-- Name: roles_id_seqq; Type: SEQUENCE SET; Schema: dee; Owner: postgres
--

SELECT pg_catalog.setval('dee.roles_id_seqq', 1, false);


--
-- Name: slin_id_seq; Type: SEQUENCE SET; Schema: dee; Owner: postgres
--

SELECT pg_catalog.setval('dee.slin_id_seq', 2, true);


--
-- Name: sprint_id_seq; Type: SEQUENCE SET; Schema: dee; Owner: postgres
--

SELECT pg_catalog.setval('dee.sprint_id_seq', 2, true);


--
-- Name: table_references_id_seq; Type: SEQUENCE SET; Schema: dee; Owner: postgres
--

SELECT pg_catalog.setval('dee.table_references_id_seq', 78, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: dee; Owner: postgres
--

SELECT pg_catalog.setval('dee.users_id_seq', 1, false);


--
-- Name: users_id_seqq; Type: SEQUENCE SET; Schema: dee; Owner: postgres
--

SELECT pg_catalog.setval('dee.users_id_seqq', 1, false);


--
-- Name: capture capture_pk; Type: CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.capture
    ADD CONSTRAINT capture_pk PRIMARY KEY (id);


--
-- Name: comment comment_pk; Type: CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.comment
    ADD CONSTRAINT comment_pk PRIMARY KEY (id);


--
-- Name: dev dev_pk; Type: CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.dev
    ADD CONSTRAINT dev_pk PRIMARY KEY (id);


--
-- Name: portfolio portfolio_pk; Type: CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.portfolio
    ADD CONSTRAINT portfolio_pk PRIMARY KEY (id);


--
-- Name: proj_details proj_details_pk; Type: CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.proj_details
    ADD CONSTRAINT proj_details_pk PRIMARY KEY (id);


--
-- Name: quality quality_pk; Type: CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.quality
    ADD CONSTRAINT quality_pk PRIMARY KEY (id);


--
-- Name: slin slin_pk; Type: CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.slin
    ADD CONSTRAINT slin_pk PRIMARY KEY (id);


--
-- Name: sprint sprint_pk; Type: CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.sprint
    ADD CONSTRAINT sprint_pk PRIMARY KEY (id);


--
-- Name: table_references table_references_pkey; Type: CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.table_references
    ADD CONSTRAINT table_references_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_un; Type: CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.users
    ADD CONSTRAINT users_un UNIQUE (user_name);


--
-- Name: capture update_capture_updated_date; Type: TRIGGER; Schema: dee; Owner: postgres
--

CREATE TRIGGER update_capture_updated_date BEFORE UPDATE ON dee.capture FOR EACH ROW EXECUTE PROCEDURE dee.updated_date_column();


--
-- Name: comment update_comment_updated_date; Type: TRIGGER; Schema: dee; Owner: postgres
--

CREATE TRIGGER update_comment_updated_date BEFORE UPDATE ON dee.comment FOR EACH ROW EXECUTE PROCEDURE dee.updated_date_column();


--
-- Name: portfolio update_portfolio_updated_date; Type: TRIGGER; Schema: dee; Owner: postgres
--

CREATE TRIGGER update_portfolio_updated_date BEFORE UPDATE ON dee.portfolio FOR EACH ROW EXECUTE PROCEDURE dee.updated_date_column();


--
-- Name: proj_details update_proj_details_updated_date; Type: TRIGGER; Schema: dee; Owner: postgres
--

CREATE TRIGGER update_proj_details_updated_date BEFORE UPDATE ON dee.proj_details FOR EACH ROW EXECUTE PROCEDURE dee.updated_date_column();


--
-- Name: quality update_quality_updated_date; Type: TRIGGER; Schema: dee; Owner: postgres
--

CREATE TRIGGER update_quality_updated_date BEFORE UPDATE ON dee.quality FOR EACH ROW EXECUTE PROCEDURE dee.updated_date_column();


--
-- Name: roles update_roles_updated_date; Type: TRIGGER; Schema: dee; Owner: postgres
--

CREATE TRIGGER update_roles_updated_date BEFORE UPDATE ON dee.roles FOR EACH ROW EXECUTE PROCEDURE dee.updated_date_column();


--
-- Name: slin update_slin_updated_date; Type: TRIGGER; Schema: dee; Owner: postgres
--

CREATE TRIGGER update_slin_updated_date BEFORE UPDATE ON dee.slin FOR EACH ROW EXECUTE PROCEDURE dee.updated_date_column();


--
-- Name: sprint update_sprint_updated_date; Type: TRIGGER; Schema: dee; Owner: postgres
--

CREATE TRIGGER update_sprint_updated_date BEFORE UPDATE ON dee.sprint FOR EACH ROW EXECUTE PROCEDURE dee.updated_date_column();


--
-- Name: users update_users_updated_date; Type: TRIGGER; Schema: dee; Owner: postgres
--

CREATE TRIGGER update_users_updated_date BEFORE UPDATE ON dee.users FOR EACH ROW EXECUTE PROCEDURE dee.updated_date_column();


--
-- Name: capture capture_proj_details_id_fkey; Type: FK CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.capture
    ADD CONSTRAINT capture_proj_details_id_fkey FOREIGN KEY (proj_details_id) REFERENCES dee.proj_details(id) ON DELETE CASCADE;


--
-- Name: comment comment_portfolio_id_fkey; Type: FK CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.comment
    ADD CONSTRAINT comment_portfolio_id_fkey FOREIGN KEY (portfolio_id) REFERENCES dee.portfolio(id) ON DELETE CASCADE;


--
-- Name: comment comment_proj_details_id_fkey; Type: FK CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.comment
    ADD CONSTRAINT comment_proj_details_id_fkey FOREIGN KEY (proj_details_id) REFERENCES dee.proj_details(id) ON DELETE CASCADE;


--
-- Name: dev dev_proj_details_id_fkey; Type: FK CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.dev
    ADD CONSTRAINT dev_proj_details_id_fkey FOREIGN KEY (proj_details_id) REFERENCES dee.proj_details(id) ON DELETE CASCADE;


--
-- Name: portfolio portfolio_capture_id_fkey; Type: FK CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.portfolio
    ADD CONSTRAINT portfolio_capture_id_fkey FOREIGN KEY (capture_id) REFERENCES dee.capture(id) ON DELETE CASCADE;


--
-- Name: portfolio portfolio_proj_details_id_fkey; Type: FK CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.portfolio
    ADD CONSTRAINT portfolio_proj_details_id_fkey FOREIGN KEY (proj_details_id) REFERENCES dee.proj_details(id) ON DELETE CASCADE;


--
-- Name: portfolio portfolio_slin_id_fkey; Type: FK CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.portfolio
    ADD CONSTRAINT portfolio_slin_id_fkey FOREIGN KEY (slin_id) REFERENCES dee.slin(id) ON DELETE CASCADE;


--
-- Name: quality quality_proj_details_id_fkey; Type: FK CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.quality
    ADD CONSTRAINT quality_proj_details_id_fkey FOREIGN KEY (proj_details_id) REFERENCES dee.proj_details(id) ON DELETE CASCADE;


--
-- Name: roles roles_users_id_fkey; Type: FK CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.roles
    ADD CONSTRAINT roles_users_id_fkey FOREIGN KEY (users_id) REFERENCES dee.users(id) ON DELETE CASCADE;


--
-- Name: slin slin_proj_details_id_fkey; Type: FK CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.slin
    ADD CONSTRAINT slin_proj_details_id_fkey FOREIGN KEY (proj_details_id) REFERENCES dee.proj_details(id) ON DELETE CASCADE;


--
-- Name: sprint sprint_proj_details_id_fkey; Type: FK CONSTRAINT; Schema: dee; Owner: postgres
--

ALTER TABLE ONLY dee.sprint
    ADD CONSTRAINT sprint_proj_details_id_fkey FOREIGN KEY (proj_details_id) REFERENCES dee.proj_details(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

